const nonProdConfig = {
    region: 'us-east-2',
    identityPoolId: 'us-east-2:8cbe849d-7449-4e9e-a364-92a123bba715',
}
const prodConfig = {
    region: 'us-east-2',
    identityPoolId: 'us-east-2:5dea6ce4-2692-4e6f-8cef-8aa27a6eb699',
}

const usedConfig = prodConfig


function testButton(environmentConfiguration) {

  let usedConfig = {}

  let usedQuerySelectors = {

  }

  if (environmentConfiguration === 'nonprod') {
    usedConfig =nonProdConfig
    usedQuerySelectors = {
      accessKey: '#accessKeyNonProd',
      secretKey: '#secretKeyNonProd',
      sessionToken: '#sessionTokenNonProd',
    }

  } else if (environmentConfiguration === 'prod') {
    usedConfig =prodConfig
    usedQuerySelectors = {
      accessKey: '#accessKeyProd',
      secretKey: '#secretKeyProd',
      sessionToken: '#sessionTokenProd',
    }
  } else {
    throw Error('undefined environmentConfiguration')
  }
    
  // Initialize the Amazon Cognito credentials provider
  AWS.config.region = usedConfig.region;
  AWS.config.credentials = new AWS.CognitoIdentityCredentials({IdentityPoolId: usedConfig.identityPoolId});

  AWS.config.credentials.get(function(err){
    if (err) {
      //Handle error
      console.error('err', err)

    } else {
      const credentials = AWS.config.credentials

      const accessKeyElement = document.querySelector(usedQuerySelectors.accessKey)
      const secretKeyElement = document.querySelector(usedQuerySelectors.secretKey)
      const sessionTokenElement = document.querySelector(usedQuerySelectors.sessionToken)

      accessKeyElement.textContent = credentials.accessKeyId
      secretKeyElement.textContent = credentials.secretAccessKey
      sessionTokenElement.textContent = credentials.sessionToken

      
    }
  });

}

  
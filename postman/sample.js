const schema = {
    type: 'object',
    properties: {
        requestContext: {
            type: 'object',
            properties: {
                auditingTag: {
                    type: 'string'
                },
                username: {
                    type: 'string'
                },
            },
            required: [
                'auditingTag',
                'username'
            ]
        },
        header: {
            type: 'object',
            properties: {
                bin: {
                    type: 'string'
                },
                pcn: {
                    type: 'string'
                },
                dateOfService: {
                    type: 'string'
                },
                overrideSetIdent: {
                    type: 'string'
                },
            },
            required: [
                'bin',
                'pcn',
                'dateOfService',
                'overrideSetIdent'
            ]
        },
        member: {
            type: 'object',
            properties: {
                dateOfBirth: {
                    type: 'string'
                },
                memberIdent: {
                    type: 'object',
                    properties: {
                        clientAccountCode: {
                            type: 'string'
                        },
                        memberNumber: {
                            type: 'string',
                        },
                        personCode: {
                            type: 'number'
                        },
                    },
                    required: [
                        'clientAccountCode',
                        'memberNumber',
                        'personCode'
                    ]
                },
                residenceCode: {
                    type: 'number'
                },
                groupNumber: {
                    type: 'string'
                },
            },
            required: [
                'dateOfBirth',
                'memberIdent',
            ]
        },
        pharmacy: {
            type: 'object',
            properties: {
                pharmacyID: {
                    type: 'object',
                    properties: {
                        npi: {
                            type: 'integer'
                        },
                        pharmacyName: {
                            type: 'string'
                        },
                    }
                },
                pharmacyServiceType: {
                    type: 'integer',
                }
            },
            required: [
                'npi'
            ]
        },
        pharmacyPricing: {
            type: 'object',
            properties: {
                usualAndCustomaryCharge: {
                    type: 'number',
                }
            },
            required: [
                'usualAndCustomaryCharge'
            ]
        },
        prescriber: {
            type: 'object',
            properties: {
                prescriberID: {
                    type: 'object',
                    properties: {
                        npi: {
                            type: 'integer',
                        },
                        prescriberLastName: {
                            type: 'string',
                        }
                    }
                }
            },
            required: [
                'npi'
            ]
        },
        prescription: {
            type: 'object',
            properties: {
                drugCode: {
                    type: 'string',
                },
                daysSupply: {
                    type: 'integer',
                },
                quantityDispensed: {
                    type: 'number',
                },
                dawCode: {
                    type: 'string',
                },
                placeOfService: {
                    type: 'integer',
                },
                quantityUnit: {
                    type: 'string',
                },
            },
            required: [
                'drugCode',
                'daysSupply',
                'quantityDispensed',
                'dawCode',
                'placeOfService',
                'quantityUnit',
            ]
        },
        clinicalList: {
            type: 'object',
            properties: {
                clinical: {
                    type: 'array',
                    items: {
                        type: 'string',
                    }
                }
            },
            required: [
                'clinical',
            ]
        },
    }
}



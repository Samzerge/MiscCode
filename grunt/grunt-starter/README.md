To call a registered task you need to use:
`grunt <taskName>`
Example:
`grunt yell`

Default task will work just with grunt:
`grunt`


To use installed plugins, Example:
`grunt.loadNpmtasks('grunt-contrib-concat')`

To set configuration for plugins:
`grunt.initConfig({
        concat: {
            dist:{
                
            }
        }
    })`
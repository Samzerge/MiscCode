

module.exports = (grunt) => {

    grunt.initConfig({
        concat: {
            dist:{
                
            }
        }
    })

    grunt.loadNpmtasks('grunt-contrib-concat')

    grunt.registerTask('speak', function() {
        console.log('Im speaking')
    })
    grunt.registerTask('yell', function() {
        console.log('Im yelling')
    })
    grunt.registerTask('shout', function() {
        console.log('Im shouting')
    })

    grunt.registerTask('both', ['speak', 'yell'])

    grunt.registerTask('default', ['shout', 'yell'])
    
}
// const utils = require('/opt/nodejs/utils')
const utils = require('./utils')

exports.getDrugFlags = async (event, context, callback) => {
  
  context.callbackWaitsForEmptyEventLoop = false
  let res = utils.constants.responseTemplate
  const timeoutToleranceMillis = process.env.TIMEOUT_TOLERANCE_MILLIS

  // const timerObject = utils.setAuxTimeoutPromise(context, callback, res, timeoutToleranceMillis)

  // const timeoutPromise = timerObject.timeoutPromise
  // const lambdaPromise = lambdaFunction(event, context, callback, res)

  let result
  try {
    console.log('race initaited')
    result = await Promise.race([
      utils.setAuxTimeoutPromise(context, callback, res, timeoutToleranceMillis).timeoutPromise,
      lambdaFunction(event, context, callback, res)
    ])
    console.log('result')

  } catch (error) {
    console.error('error in race promise', error)
    return error
  }
  return result

}

const lambdaFunction = async (event, context, callback, res) => {
  
  console.log('lambdaFunction started')
  let resultObject = null
  
  const requestParams = {
    drugNameParam: {
        name: 'name',
        required: true,
        multipleValues: false,
        validations: null
    },
  }
  
  const validationResult = utils.validateRequest(event, res, requestParams)
  if (!validationResult.isValid) {
    resultObject = validationResult.res
    return resultObject

  }

  const secretsManager = utils.getSecretsManager(process.env.SECRET_MANAGER_REGION)
  
  const requestStage = event.requestContext.stage
  
  const databaseDeploymentStage = utils.getDatabaseMappedFromDeploymentStage(requestStage)

  const pool = await utils.getMySQLConnectionPool(secretsManager, process.env.RDS_CREDENTIALS_SECRET_ID, {
    host     : process.env.RDS_HOSTNAME,
    port     : process.env.RDS_PORT,
    database : databaseDeploymentStage
  })
  console.log('pool retrieved')
  console.log('pool', pool)

  try {

    const dbResult = await utils.getPoolQueryPromise(pool, "CALL drug_lookup_flags_get(?)", [event.queryStringParameters[requestParams.drugNameParam.name]])
    console.log('dbResult', dbResult)

    if (dbResult[0].length <= 0 || dbResult[0][0] == null || dbResult[0][0].length <= 0) {
        console.error('err in dbResult', dbResult) 
        res = utils.createErrorObject(res, utils.constants.httpCodes.NOT_FOUND, `Result not found: '${requestParams.drugNameParam.name}: ${event.queryStringParameters[requestParams.ndcParam.name]}'`)
        
    } else {

      const dbItems = dbResult[0]

      const casedResults = dbItems.map(item => utils.convertSnakeToCamelCase(item))
      
      res.statusCode = 200
      res.body = JSON.stringify(casedResults)
    }
  } catch(err) {
    console.error('err', err)
    res.statusCode = 500
    res.body = JSON.stringify({
        message: err
    })
    
  } finally {
    // utils.finishCall(callback, res, timer)

    resultObject = res
    return resultObject


  }
}
const mysql = require('mysql')
const AWS = require('aws-sdk')
const databasePools = [{
    id: -1,
    pool: null
}]
const timerPools = []
const initialized  = ()  =>{
    console.log('initialized')
    databasePools.forEach(poolObj => {

        if (poolObj.id !== -1) {
            console.log('closing pool with id = ' + poolObj.id)
            poolObj.pool.end()
        }
    })
}
initialized()


const constants = {

    responseTemplate: {
        "headers": {
              "Content-Type": "*/*",
              "Access-Control-Allow-Origin": "*"
        },
        "isBase64Encoded": false
    },
    httpCodes: {
        BAD_REQUEST: 400,
        NOT_FOUND: 404,
        NOT_IMPLEMENTED: 500,
    }

}

let interval = null
/**
 * Creates an auxiliary function that helps to send an error message when Lambda timeouts
 * @param {*} context 
 * @param {*} callback 
 * @param {*} res 
 * @param {*} timeoutToleranceMillis 
 * @returns 
 */
function setAuxTimeout(context, callback, res, timeoutToleranceMillis){
    console.log('setAuxTimeout')
    console.log('context.getRemainingTimeInMillis()', context.getRemainingTimeInMillis())
    console.log('timeoutToleranceMillis', timeoutToleranceMillis)
    console.log('context.getRemainingTimeInMillis() - timeoutToleranceMillis', context.getRemainingTimeInMillis() - timeoutToleranceMillis)


    let interval
    let timer
    const id = Date.now()
    const timeoutPromise = new Promise((resolve, reject) => {

        console.log('timeout promise initaited')
        let intervalMillis = 0
        interval = setInterval(() => {

            intervalMillis += 50
            console.log(`id:${id} - setInterval tick`, intervalMillis)
            
        
        }, 50)
        
        timer = setTimeout(() => {
            console.log('timed out entered')
            res.statusCode = 408
            res.body = JSON.stringify({
                code: 408,
                status: "Lambda function timeout",
                message: `Lambda function timeout`
            })

            const rejectObject = {
                callback,
                res,
                timer
            }
            reject(rejectObject)
            // finishCall(callback,res, timer)
        
        }, context.getRemainingTimeInMillis() - timeoutToleranceMillis)

    }).then(() => {
        console.log('this should never run')
    }).catch(err => {
        
        console.error('timeout error', error)
    })


    const timerObject = {
        id,
        timer,
        finished: false,
        interval,
        timeoutPromise
    }

    console.log('timerObject', timerObject)

    // const timer = setTimeout(() => {
    //     console.log('timed out entered')
    //     res.statusCode = 408
    //     res.body = JSON.stringify({
    //         code: 408,
    //         status: "Lambda function timeout",
    //         message: `Lambda function timeout`
    //     })
    //     finishCall(callback,res, timer)
    
    // }, context.getRemainingTimeInMillis() - timeoutToleranceMillis)

    // let intervalMillis = 0
    // const interval = setInterval(() => {

    //     intervalMillis += 50
    //     console.log('setInterval tick', intervalMillis)
        
    
    // }, 50)
    // const timerObject = {
    //     id: Date.now(),
    //     timer,
    //     finished: false,
    //     interval
    // }

    timerPools.push(timerObject)
    
    return timerObject
}

const setAuxTimeoutPromise = (context, callback, res, timeoutToleranceMillis) => {
    let interval
    let timer
    let timerObject
    const id = Date.now()
    const timeoutTime = context.getRemainingTimeInMillis() - timeoutToleranceMillis

    console.log('timeoutTime', timeoutTime)
    const timeoutPromise = new Promise((resolve, reject) => {

        console.log('timeout promise initaited')
        
        timer = setTimeout(() => {
            console.log('timed out entered')
            res.statusCode = 408
            res.body = JSON.stringify({
                code: 408,
                status: "Lambda function timeout",
                message: `Lambda function timeout`
            })

            const rejectObject = {
                res,
                timerObject
            }
            clearTimerObject(timerObject)
            reject(rejectObject)

        
        }, timeoutTime)

    })

    timerObject = {
        id,
        timer,
        finished: false,
        interval,
        timeoutPromise
    }

    console.log('timerObject', timerObject)

    // const timer = setTimeout(() => {
    //     console.log('timed out entered')
    //     res.statusCode = 408
    //     res.body = JSON.stringify({
    //         code: 408,
    //         status: "Lambda function timeout",
    //         message: `Lambda function timeout`
    //     })
    //     finishCall(callback,res, timer)
    
    // }, context.getRemainingTimeInMillis() - timeoutToleranceMillis)

    // let intervalMillis = 0
    // const interval = setInterval(() => {

    //     intervalMillis += 50
    //     console.log('setInterval tick', intervalMillis)
        
    
    // }, 50)
    // const timerObject = {
    //     id: Date.now(),
    //     timer,
    //     finished: false,
    //     interval
    // }

    timerPools.push(timerObject)
    
    return timerObject
}

/**
 * Clears timeout and informs lambda that execution has finished
 * @param {*} callback 
 * @param {*} responseObject 
 * @param {*} timer 
 */
function finishCall(callback, responseObject, timerObject){
    console.log('finishCall')
    // clearInterval(interval)
    clearTimerObject(timerObject)
    callback(null, responseObject)
}

const clearTimerObject = (timerObject) => {
    console.log('clearTimerObject')
    
    const timerObjectIndex = timerPools.findIndex(timerPoolObj => {
        return timerPoolObj.id === timerObject.id
    })
    console.log('timerObjectIndex', timerObjectIndex)
    
    if (timerObjectIndex !== -1) {
        console.log('found timer in pool')
        const foundTimerObject = timerPools[timerObjectIndex]
        clearTimeout(foundTimerObject.timer)
        clearInterval(foundTimerObject.interval)
        foundTimerObject.finished = true
        timerPools.splice(timerObjectIndex, 1)
        console.log('timerPools after splice', [...timerPools])
    } else {
        console.log('timer not in pool')
        console.log('ttimerObject', timerObject)
        clearTimeout(timerObject.timer)
        clearInterval(timerObject.interval)
        timerObject.finished = true
    }
    
    console.log('clearTimerObject finished')

}
  
/**
 * Validates every parameter accepted in the API
 * @param {*} event 
 * @param {*} paramsArray 
 */
function validateRequest(event, res, requestParams){

    for(const paramKey in requestParams) {
        const param = requestParams[paramKey]
        
        let requestValue = null

        if (event.queryStringParameters != null) {
            requestValue = param.multipleValues ? event.multiValueQueryStringParameters[param.name] : event.queryStringParameters[param.name]
        }

        if(param.required && (requestValue == null || requestValue === '')) {
            return createValidityErrorObject(res, constants.httpCodes.BAD_REQUEST, `Missing required request parameters: '${param.name}'.`)
        }
        
        if (requestValue != null && requestValue !== '' && param.validations != null) {
            for (let validationKey in param.validations) {
                const referenceValue = param.validations[validationKey]

                switch(validationKey) {
                    case 'acceptedValues':

                        if (param.multipleValues) {
                        
                            for(const value of requestValue) {                  
                                if(!referenceValue.includes(value)) {
                                    return createValidityErrorObject(res, constants.httpCodes.BAD_REQUEST, `Parameter '${param.name}' only accepts the following values: [${referenceValue}].`)
                                }
                            }
                        
                        
                        } else {
                            if(event.multiValueQueryStringParameters[param.name].length > 1 || !referenceValue.includes(requestValue)) {
                                return createValidityErrorObject(res, constants.httpCodes.BAD_REQUEST, `Parameter '${param.name}' only accepts 1 of the following values: ${referenceValue}.`)
                            }
                        }
                    break;
                    case 'minLength':
                        if (requestValue.length < referenceValue) {
                            return createValidityErrorObject(res, constants.httpCodes.BAD_REQUEST, `Parameter '${param.name}' must have at least ${referenceValue} characters.`)
                        }
                    break;
                    case 'digitsOnly':
                        if (requestValue.match(/^\d+$/) == null) {
                            return createValidityErrorObject(res, constants.httpCodes.BAD_REQUEST, `Parameter '${param.name}' can only contain digits.`)
                        }
                    break;
                }
            }
        }
    }
    return {
        isValid: true,
        res: res
    }
}

/**
 * Create json filter for storec procedures based on request param values or environment variables
 * @param {*} multiValueQueryStringParameters  the container for the multiValue Parameters
 * @param {*} env  the container for the enviornment variables
 * @returns 
 */
function buildFilterJsonFromParams(multiValueQueryStringParameters, env) {
    const controlledSubtance = multiValueQueryStringParameters[process.env.CONTROLLED_SUBSTANCE_PARAM]
    const itemStatus = multiValueQueryStringParameters[process.env.ITEM_STATUS_PARAM]
    const deaClass = multiValueQueryStringParameters[process.env.DEA_CLASS_PARAM]
    const rxOtc = multiValueQueryStringParameters[process.env.RTX_INDICATOR_PARAM]
  
    return {
      controlled_substance_code: controlledSubtance == null ? env.CONTROLLED_SUBSTANCE_CODES_DEFAULT : controlledSubtance.join(','),
      item_status_flag: itemStatus == null ? env.ITEM_STATUS_FLAGS_DEFAULT : itemStatus.join(','),
      dea_class_code: deaClass == null ? env.DEA_CLASS_CODE_DEFAULT : deaClass.join(','),
      rx_otc_indicator_code: rxOtc == null ? env.RTX_INDICATOR_CODE_DEFAULT : rxOtc.join(',')
    }
}

function createValidityErrorObject(res, httpCode, message) {
    
    return {
        isValid: false,
        res: createErrorObject(res, httpCode, message)
    }
}

function createErrorObject(res, httpCode, message) {
    let status = ''
    switch(httpCode) {
        case constants.httpCodes.BAD_REQUEST:
            status = 'Bad Request'
        break
        case constants.httpCodes.NOT_IMPLEMENTED:
            status = 'Not Implemented'
        break
        case constants.httpCodes.NOT_FOUND:
            status = 'Not Found'
        break
    }

    res.statusCode = httpCode
    res.body = JSON.stringify({
        code: httpCode,
        status: status,
        message: message
    })

    return res
}

const toCamelCase = (text) => {
    return text.replace(/([-_][a-z])/ig, (snakeCase) => {
      return snakeCase.toUpperCase()
        .replace('-', '')
        .replace('_', '')
    })
}

const convertSnakeToCamelCase= (obj) => {

    if (typeof obj !== 'object' || obj == null) {
        return obj
    } 

    const aux = {}
    Object.keys(obj)
        .forEach((key) => {
            aux[toCamelCase(key)] = convertSnakeToCamelCase(obj[key])
        })

    return aux
}

const getPoolQueryPromise = (pool, query, values) => {
    return new Promise((resolve, reject) => {
        pool.query(query, values,(err, result, fields) => {
        if (err) {
            return reject(err)
        }

        return resolve(result)
        })
    })
}

let secretsManager = null
const getSecretsManager = (secretsManagerRegion) => {

    if (secretsManager == null) {
        secretsManager = new AWS.SecretsManager({ region: secretsManagerRegion})
    }
    return secretsManager
}

const getAWS = () => {
    return AWS
}

/**
 * Wrapper that promisifies secretsManager.getSecretValue to get value stored on a secret on the Secrets Manager
 * @param {AWS.SecretsManager} secretsManager 
 * @param {string} secretId 
 * @returns 
 */
const getSecretValuePromise = (secretsManager, secretId) => {
    return new Promise((resolve, reject) => {
        secretsManager.getSecretValue({
        SecretId: secretId
        }, function(err, data) {
        if (err) {
            reject(err)
        } else {
            resolve(data)
        }
        })
    })
}

/**
 * Gets the secret value stored in a secret object depending on the secret type (SecretString  | SecretBinary)
 * @param {{'SecretString' | 'SecretBinary'}} expectedSecretType 
 * @param {{SecretString: string, SecretBinary: string}} secretObject 
 * @returns 
 */
const getSecretValue = (expectedSecretType, secretObject) => {
    if (secretObject == null)
        throw Error('Secret object was not provided')


    if (expectedSecretType === 'SecretString') {
        if (expectedSecretType in secretObject) {
        return secretObject.SecretString
        }
        throw Error(`Expected ${expectedSecretType} but value was not found in Secret`)

    } else if (expectedSecretType === 'SecretBinary') {
    //   throw Error(`Unimplemendted secret type`)

        if (expectedSecretType in secretObject) {
        return getSecretBinary(secretObject.SecretBinary)
        }
        throw Error(`Expected ${expectedSecretType} but value was not found in Secret`)

    } else {
        throw Error('Expected secret type is not valid')
    }
}

const getSecretBinary = (secretBinary) => {
    // throw Error('SecretBinary handling not implemented')
    let buff = new Buffer(secretBinary, 'base64')
    // decodedBinarySecret = buff.toString('ascii')
    // console.log('decodedBinarySecret', decodedBinarySecret)

    return buff

}

/**
 * Wrapper that promisifies secretsManager.updateSecret to update the value stored on a secret on the Secrets Manager
 * @param {AWS.SecretsManager} secretsManager 
 * @param {{SecretId: string, SecretString: string}} updateRequestObject 
 * @returns 
 */
 const updateSecretPromise = (secretsManager, updateRequestObject) => {
  
    return new Promise ((resolve, reject) => {
    
      secretsManager.updateSecret(updateRequestObject, (err, data) => {
        if (err) {
          reject(err)
        } else {
          resolve(data)
        }
      })
    })
  }


  /**
 * 
 * @param {*} secretsManager 
 * @param {*} dbCredentialsSecretId 
 * @param {*} dbOptions {
    host,
    port,
    database
}
 * @returns 
 */
const getMySQLConnectionPool = async (secretsManager, dbCredentialsSecretId, dbOptions) => {

    const dbCredentialsSecret = await getSecretValuePromise(secretsManager, dbCredentialsSecretId)
    
    const dbCredentialsSecretValue = getSecretValue('SecretString', dbCredentialsSecret)
    const jsonSecret  = JSON.parse(dbCredentialsSecretValue)

    const credentialsObject = {
        database: dbOptions.database,
        host: dbOptions.host,
        port: dbOptions.port,
        user: jsonSecret.DBusername,
        password: jsonSecret.DBpassword,
    }
    console.log('databasePools', databasePools)

    const poolId = Object.values(credentialsObject).reduce((previousValue, currentValue) => {
        return previousValue + '' + currentValue
    }, '')

    console.log('poolId', poolId)
    
    const filteredDbPools = databasePools.filter(dbPool => {
        return dbPool.id === poolId
    })
    console.log('filteredDbPools', filteredDbPools)

    if (filteredDbPools.length > 0) {
        console.log('pool connection found')
        pool = filteredDbPools[0].pool
    } else {
        console.log('pool not found, creating new one')
        pool =  mysql.createPool(credentialsObject)
        databasePools.push({
            id: poolId,
            pool
        })
    }

    console.log('pool', pool)

    return pool
    
}

const getDatabaseMappedFromDeploymentStage = (apiStage) => {

    const defaultDatabase = 'medispan_drug'
    if (process.env.RDS_DATABASE_STAGE != null) {
        database = process.env.RDS_DATABASE_STAGE
    } else if (process.env.RDS_DATABASE_PROD != null) {
        database = process.env.RDS_DATABASE_PROD
    } else {
        console.error('Default stage has not been found on current deployed environment')
    }

    switch(apiStage) {
        case 'dev':
            database = process.env.RDS_DATABASE_DEV
            break
        case 'qa':
        case 'test':
            database =  process.env.RDS_DATABASE_QA
            break
        case 'stage':
            database = process.env.RDS_DATABASE_STAGE
            break
        case 'beta':
        case 'prod':
        case 'v1':
            database = process.env.RDS_DATABASE_PROD
            break
        default:
            console.error('Stage has not been mapped to a database')
            database = defaultDatabase
    }
    return database
}


module.exports.constants = constants
module.exports.setAuxTimeout = setAuxTimeout
module.exports.setAuxTimeoutPromise = setAuxTimeoutPromise
module.exports.finishCall = finishCall
module.exports.validateRequest = validateRequest
module.exports.buildFilterJsonFromParams = buildFilterJsonFromParams
module.exports.createErrorObject = createErrorObject
module.exports.convertSnakeToCamelCase = convertSnakeToCamelCase
module.exports.getPoolQueryPromise = getPoolQueryPromise
module.exports.getSecretValuePromise = getSecretValuePromise
module.exports.updateSecretPromise = updateSecretPromise
module.exports.getSecretValue = getSecretValue
module.exports.getMySQLConnectionPool = getMySQLConnectionPool
module.exports.getSecretsManager = getSecretsManager
module.exports.getAWS = getAWS
module.exports.getDatabaseMappedFromDeploymentStage = getDatabaseMappedFromDeploymentStage


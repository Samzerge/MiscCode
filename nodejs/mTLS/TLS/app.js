const axios = require('axios')
const agent = require('./agent');

const agentA = 'alice'
const agentB = 'bob'

let httpsAgentOptions = { httpsAgent: agent(agentB) };


const options = {
    url: 'https://localhost:4433/authenticate',
    headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
    },
    method: 'GET',
    ...httpsAgentOptions
}

const main = async () => {
    let result = 'None'
    try {

        result = await axios({...options})
        console.log(result.data);
    } catch(err) {

        console.error(err.response.data);
        // console.error(err)

    }
}

main()


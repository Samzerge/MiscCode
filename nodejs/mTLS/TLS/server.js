const path = require('path')
const fs = require('fs')
const https = require('https')
const tls = require('tls')
const express = require('express')

const app = express()

const options = {
    key: fs.readFileSync(path.join(__dirname, 'certificates/server/server_key.pem')),
	cert: fs.readFileSync(path.join(__dirname, 'certificates/server/server_cert.pem')),
	requestCert: true,
	rejectUnauthorized: false, // so we can do own error handling
	ca: [
		fs.readFileSync(path.join(__dirname, 'certificates/server/server_cert.pem'))
	]
}


app.get('/', (req, res) => {
	res.send('<a href="/authenticate">Log in using client certificate</a>');
})

app.get('/authenticate', (req, res) => {
	const cert = req.socket.getPeerCertificate();

    console.log('authenticating')
	if (req.client.authorized) {
		res.send(`Hello ${cert.subject.CN}, your certificate was issued by ${cert.issuer.CN}!`);

	} else if (cert.subject) {
		res.status(403)
			 .send(`Sorry ${cert.subject.CN}, certificates from ${cert.issuer.CN} are not welcome here.`);

	} else {
		res.status(401)
		   .send(`Sorry, but you need to provide a client certificate to continue.`);
	}
})

https.createServer(options, app).listen(4433, () => {
	const msg = `SERVER ONLINE at https://localhost:4433`
	console.log(msg)
})

// app.use(function (req, res, next) {
//     if (!req.client.authorized) {
//         console.error('!req.client.authorized')
//         return null;
//       //return res.status(401).send('Client cert failed. User is not authorized\n');
//     }
//     // Examine the cert itself, and even validate based on that!
//     const cert = req.socket.getPeerCertificate();
//     if (cert.subject) {
//       console.log('Client Certificate: ',cert);
//       console.log('Client Certificate Common Name: '+cert.subject.CN);
//       console.log('Client Certificate Location: '+cert.subject.L);
//       console.log('Client Certificate Organization Name: '+cert.subject.O);
//       console.log('Client Certificate Email Address: '+cert.subject.emailAddress);
//     }
  
//     res.writeHead(200, {'Content-Type': 'text/plain'});
//     res.end("hello world from client cert\n");
//     next();
//   })
//   const listener = https.createServer(options, app).listen(4433, function () {
//     console.log('Express HTTPS server listening on port ' + listener.address().port);
//   })
      
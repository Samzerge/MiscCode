const axios = require('axios')

const agent = require('./agent');


let httpsAgentOptions = { httpsAgent: agent(true) };


const options = {
    url: 'https://localhost:4433/authenticate',
    headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
    },
    method: 'GET',
    ...httpsAgentOptions
}

const main = async () => {
    let result = 'None'
    try {

        result = await axios({...options})
        console.log(result.data);
    } catch(err) {

        console.error(err)
        console.error(err.response.data);

    }
}

main()

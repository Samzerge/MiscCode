const path = require('path')
const fs = require('fs')
const https = require('https')
const tls = require('tls')
const express = require('express')

const app = express()

const options = { 
    ca: fs.readFileSync('certificates/ca.crt'), 
    cert: fs.readFileSync('certificates/server/server.crt'), 
    key: fs.readFileSync('certificates/server/server.key'), 
    rejectUnauthorized: true,
    requestCert: true, 
}

app.get('/', (req, res) => {
	res.send('<a href="/authenticate">Log in using client certificate</a>');
})

app.get('/authenticate', (req, res) => {
	const cert = req.socket.getPeerCertificate();

    console.log('authenticating')
	if (req.client.authorized) {
		res.send(`Hello ${cert.subject.CN}, your certificate was issued by ${cert.issuer.CN}!`);

	} else if (cert.subject) {
		res.status(403)
			 .send(`Sorry ${cert.subject.CN}, certificates from ${cert.issuer.CN} are not welcome here.`);

	} else {
		res.status(401)
		   .send(`Sorry, but you need to provide a client certificate to continue.`);
	}
})

https.createServer(options, app).listen(4433, () => {
	const msg = `SERVER ONLINE at https://localhost:4433
To see demo, run in a new session:`
	console.log(msg)
})

      
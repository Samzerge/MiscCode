const https = require('https');
const fs = require('fs');

module.exports = function (successSample) {

	const valueInCa = fs.readFileSync('certificates/ca.crt')
	const valueInCrt = fs.readFileSync('certificates/client/client.crt')
	const valueInCk = fs.readFileSync('certificates/client/client.key')
	
	console.log('valueInCa', valueInCa)
	console.log('valueInCrt', valueInCrt)
	console.log('valueInCk', valueInCk)
	
	const agentProperties = {
        ca: fs.readFileSync('certificates/ca.crt'), 
        rejectUnauthorized: false,
    }

	if (successSample) {

		agentProperties.cert = fs.readFileSync('certificates/client/client.crt')
		agentProperties.key = fs.readFileSync('certificates/client/client.key')
	} 

	return new https.Agent({...agentProperties})
	
}
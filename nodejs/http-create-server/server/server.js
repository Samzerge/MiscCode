const http = require('http')
const express = require("express")
const app = express()
const url = require('url')
const PORT = 8080
const ADDRESS = '127.0.0.1'

var secretsCache = {
    'harcoded1': 'The best secret ever',
    'harcoded2': 'The worst secret ever'
};
var cacheLastUpdated;

async function processMyPayload(req, res) {
    console.log('processMyPayload')

    const queryObject = url.parse(req.url,true).query;

    console.log('req', req)
    console.log('req.url', req.url)
    console.log('queryObject', queryObject)

    const secretName = queryObject.secretId
                // const secretName = request.params['harcoded1']
    let secretValue = secretsCache[secretName]

    if (secretValue == null) {
        secretValue = 'undefined secret id'
    }
    res.setHeader("Content-Type", "application/json");
    res.status(200);
    res.end(secretValue);
}

async function startHttpServer() {
    console.log('startHttpServer with express')
    app.get("/cache/:name", function (req, res) {
        console.log('received cache/:name request')
        return processPayload(req, res);
    });
    app.get("/my-cache-value", function (req, res) {
        console.log('received my cahche value request')
        return processMyPayload(req, res);
    });
    
    app.listen(PORT, ADDRESS, function (error) {
        if (error) throw error
        console.log("Server created Successfully on PORT", PORT)
    });
    

    console.log(`Listening for requests at http://${ADDRESS}:${PORT}`);
}
module.exports = {
    startHttpServer
};
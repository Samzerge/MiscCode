const axios = require('axios')

const main = async () => {

  const myUrl = 'http://127.0.0.1:8080/my-cache-value'
  
  try {
    const result = await axios.get(myUrl, {
      params: {
        secretId: 'harcoded1'
      }
    })
    console.log('result', result.data)
    return result.data

  } catch(error) {
    console.log('error', error)
      return error
  }

}
main()
  
const axios = require('axios')

// const URL_PARAMS = new URLSearchParams(window.location.search);
// const TOKEN = URL_PARAMS.get('token');

// // Show an element
// const show = (selector) => {
//   document.querySelector(selector).style.display = 'block';
// };

// // Hide an element
// const hide = (selector) => {
//   document.querySelector(selector).style.display = 'none';
// };

// if (TOKEN) {
//   hide('.content.unauthorized');
//   show('.content.authorized');
// }






// let httpsAgentOptions = { httpsAgent: agent(true) };
const options = {
    url: 'https://localhost:3000/auth',
    headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
    },
    method: 'GET',
    // ...httpsAgentOptions
}

const main = async () => {
    let result = 'None'
    try {

        result = await axios({...options})
        console.log(result.data);
    } catch(err) {

        console.error(err)
        // console.error(err.response.data);

    }
}

main()

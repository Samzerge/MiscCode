
const axios = require('axios')
const agent = require('./agent')



const FormData = require('form-data')
const fs = require('fs')
const https = require('https')

const credentialAgent = { 
    httpsAgent: new https.Agent({
        ca: fs.readFileSync('certificates/exampleB/ca.crt'), 
        cert: fs.readFileSync('certificates/exampleB/client/client.crt'), 
        key: fs.readFileSync('certificates/exampleB/client/client.key'), 
        rejectUnauthorized: false,
    })
}

const clientId = ''
const clientSecret = ''
const grantType = 'client credentials'
const scope = 'openid'



const main = async () => {
    let result = ''
    const empty = false
    const url = ''
    const path = ''
    const res =  {}

    try {

    
        // const authResult = await auth()
        const authResult = await authRequest()

        const accessToken = authResult['access_token']
        if (accessToken == null || accessToken === '') {
            // TODO: Create error object
            res.statusCode = 400

        } else {
           const pricingResult = await pricingRequest(accessToken)
           res.statusCode = 200
           res.body = JSON.stringify({
               result: 'Hello'
           })
        }
    } catch(err) {
        console.error('err', err)
        res.statusCode = 500
        res.body = JSON.stringify({
            message: err
        })
        
    } finally {
        // console.log('result', result)
    }
}

const authRequest = async() => {
    console.log('auth')
    
    const responseModel = {
        'access_token': '',
        'id_token': '',
        'grant_id': '',
        'expires_in': 7200,
        'token_type': 'Bearer',
        'scope': 'openid'
    }

    const formData = new FormData()
    formData.append('client_id', clientId)
    formData.append('client_secret', clientSecret)
    formData.append('grant_type', grantType)
    formData.append('scope', scope)
    
    

    // const finalHeaders = {
    //     ...headers,
    //     ...formData.getHeaders(),
    // }
    // console.log('finalHeaders', finalHeaders)


    const authRequestOptions = {
        url: 'https://prod1medimpact.verify.ibm.com',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/x-www-form-urlencoded'
        },
        data: formData,
        method: 'POST',
        ...httpsAgentOptions
    }

    
    // const authResult = await axios.post(options.path, form_data, { headers:finalHeaders })
    const authResult = await axios({...authRequestOptions})
    
    console.log('authResult', authResult)

    return authResult
    
}

const pricingRequest =  async (accessToken) => {
    console.log('pricingRequest')

    const pricingRequestOptions = {
        url: 'https://api-strict.medimpact.com',
        headers: {
            'Host': 'api-strict.medimpact.com',
            'Content-Type': 'application/x-www-form-urlencoded',
            'authorization': `Bearer ${accessToken}`
        },
        method: 'GET',
        ...httpsAgentOptions
    }
    
    // const result = await axios.get(...options)
    let result = 'None'
    result = await axios({...pricingRequestOptions})

    return result
    
}

main()




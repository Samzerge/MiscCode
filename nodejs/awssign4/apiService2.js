'use strict';
// import * as AWS from "aws-sdk"
// import * as aws4 from 'aws4'
// import * as fetch from "node-fetch"

// import SHA256 from "crypto-js/sha256";
// import encHex from "crypto-js/enc-hex";
// import HmacSHA256 from "crypto-js/hmac-sha256";


const AWS = require("aws-sdk")
const aws4 = require('aws4')
const fetch = require("node-fetch")
const SHA256 = require("crypto-js/sha256")
const encHex = require("crypto-js/enc-hex")
const HmacSHA256 = require("crypto-js/hmac-sha256")

const sigV4Client = {};
sigV4Client.newClient = function(config) {
  const AWS_SHA_256 = "AWS4-HMAC-SHA256";
  const AWS4_REQUEST = "aws4_request";
  const AWS4 = "AWS4";
  const X_AMZ_DATE = "x-amz-date";
  const X_AMZ_SECURITY_TOKEN = "x-amz-security-token";
  const HOST = "host";
  const AUTHORIZATION = "Authorization";

  function hash(value) {
    return SHA256(value); // eslint-disable-line
  }

  function hexEncode(value) {
    return value.toString(encHex);
  }

  function hmac(secret, value) {
    return HmacSHA256(value, secret, { asBytes: true }); // eslint-disable-line
  }

  function buildCanonicalRequest(method, path, queryParams, headers, payload) {
    return (
      method +
      "\n" +
      buildCanonicalUri(path) +
      "\n" +
      buildCanonicalQueryString(queryParams) +
      "\n" +
      buildCanonicalHeaders(headers) +
      "\n" +
      buildCanonicalSignedHeaders(headers) +
      "\n" +
      hexEncode(hash(payload))
    );
  }

  function hashCanonicalRequest(request) {
    return hexEncode(hash(request));
  }

  function buildCanonicalUri(uri) {
    return encodeURI(uri);
  }

  function buildCanonicalQueryString(queryParams) {
    if (Object.keys(queryParams).length < 1) {
      return "";
    }

    let sortedQueryParams = [];
    for (let property in queryParams) {
      if (queryParams.hasOwnProperty(property)) {
        sortedQueryParams.push(property);
      }
    }
    sortedQueryParams.sort();

    let canonicalQueryString = "";
    for (let i = 0; i < sortedQueryParams.length; i++) {
      canonicalQueryString +=
        sortedQueryParams[i] +
        "=" +
        encodeURIComponent(queryParams[sortedQueryParams[i]]) +
        "&";
    }
    return canonicalQueryString.substr(0, canonicalQueryString.length - 1);
  }

  function buildCanonicalHeaders(headers) {
    let canonicalHeaders = "";
    let sortedKeys = [];
    for (let property in headers) {
      if (headers.hasOwnProperty(property)) {
        sortedKeys.push(property);
      }
    }
    sortedKeys.sort();

    for (let i = 0; i < sortedKeys.length; i++) {
      canonicalHeaders +=
        sortedKeys[i].toLowerCase() + ":" + headers[sortedKeys[i]] + "\n";
    }
    return canonicalHeaders;
  }

  function buildCanonicalSignedHeaders(headers) {
    let sortedKeys = [];
    for (let property in headers) {
      if (headers.hasOwnProperty(property)) {
        sortedKeys.push(property.toLowerCase());
      }
    }
    sortedKeys.sort();

    return sortedKeys.join(";");
  }

  function buildStringToSign(
    datetime,
    credentialScope,
    hashedCanonicalRequest
  ) {
    return (
      AWS_SHA_256 +
      "\n" +
      datetime +
      "\n" +
      credentialScope +
      "\n" +
      hashedCanonicalRequest
    );
  }

  function buildCredentialScope(datetime, region, service) {
    return (
      datetime.substr(0, 8) + "/" + region + "/" + service + "/" + AWS4_REQUEST
    );
  }

  function calculateSigningKey(secretKey, datetime, region, service) {
    return hmac(
      hmac(
        hmac(hmac(AWS4 + secretKey, datetime.substr(0, 8)), region),
        service
      ),
      AWS4_REQUEST
    );
  }

  function calculateSignature(key, stringToSign) {
    return hexEncode(hmac(key, stringToSign));
  }

  function extractHostname(url) {
    var hostname;

    if (url.indexOf("://") > -1) {
      hostname = url.split('/')[2];
    }
    else {
      hostname = url.split('/')[0];
    }

    hostname = hostname.split(':')[0];
    hostname = hostname.split('?')[0];

    return hostname;
  }

  function buildAuthorizationHeader(
    accessKey,
    credentialScope,
    headers,
    signature
  ) {
    return (
      AWS_SHA_256 +
      " Credential=" +
      accessKey +
      "/" +
      credentialScope +
      ", SignedHeaders=" +
      buildCanonicalSignedHeaders(headers) +
      ", Signature=" +
      signature
    );
  }

  let awsSigV4Client = {};
  if (config.accessKey === undefined || config.secretKey === undefined) {
    return awsSigV4Client;
  }
  awsSigV4Client.accessKey = config.accessKey;
  awsSigV4Client.secretKey = config.secretKey;
  awsSigV4Client.sessionToken = config.sessionToken;
  awsSigV4Client.serviceName = config.serviceName || "execute-api";
  awsSigV4Client.region = config.region || "us-east-1";
  awsSigV4Client.defaultAcceptType =
    config.defaultAcceptType || "application/json";
  awsSigV4Client.defaultContentType =
    config.defaultContentType || "application/json";

  const invokeUrl = config.endpoint;
  const endpoint = /(^https?:\/\/[^/]+)/g.exec(invokeUrl)[1];
  const pathComponent = invokeUrl.substring(endpoint.length);

  awsSigV4Client.endpoint = endpoint;
  awsSigV4Client.pathComponent = pathComponent;

  awsSigV4Client.signRequest = function(request) {
    const verb = request.method.toUpperCase();
    const path = awsSigV4Client.pathComponent + request.path;
    const queryParams = { ...request.queryParams };
    const headers = { ...request.headers };

    // If the user has not specified an override for Content type the use default
    if (headers["Content-Type"] === undefined) {
      headers["Content-Type"] = awsSigV4Client.defaultContentType;
    }

    // If the user has not specified an override for Accept type the use default
    if (headers["Accept"] === undefined) {
      headers["Accept"] = awsSigV4Client.defaultAcceptType;
    }

    let body = { ...request.body };
    // override request body and set to empty when signing GET requests
    if (request.body === undefined || verb === "GET") {
      body = "";
    } else {
      body = JSON.stringify(body);
    }

    // If there is no body remove the content-type header so it is not
    // included in SigV4 calculation
    if (body === "" || body === undefined || body === null) {
      delete headers["Content-Type"];
    }

    let datetime = new Date()
      .toISOString()
      .replace(/\.\d{3}Z$/, "Z")
      .replace(/[:-]|\.\d{3}/g, "");
    headers[X_AMZ_DATE] = datetime;
    headers[HOST] = extractHostname(awsSigV4Client.endpoint);

    let canonicalRequest = buildCanonicalRequest(
      verb,
      path,
      queryParams,
      headers,
      body
    );
    let hashedCanonicalRequest = hashCanonicalRequest(canonicalRequest);
    let credentialScope = buildCredentialScope(
      datetime,
      awsSigV4Client.region,
      awsSigV4Client.serviceName
    );
    let stringToSign = buildStringToSign(
      datetime,
      credentialScope,
      hashedCanonicalRequest
    );
    let signingKey = calculateSigningKey(
      awsSigV4Client.secretKey,
      datetime,
      awsSigV4Client.region,
      awsSigV4Client.serviceName
    );
    let signature = calculateSignature(signingKey, stringToSign);
    headers[AUTHORIZATION] = buildAuthorizationHeader(
      awsSigV4Client.accessKey,
      credentialScope,
      headers,
      signature
    );
    if (
      awsSigV4Client.sessionToken !== undefined &&
      awsSigV4Client.sessionToken !== ""
    ) {
      headers[X_AMZ_SECURITY_TOKEN] = awsSigV4Client.sessionToken;
    }
    delete headers[HOST];

    let url = awsSigV4Client.endpoint + path;
    let queryString = buildCanonicalQueryString(queryParams);
    if (queryString !== "") {
      url += "?" + queryString;
    }

    // Need to re-attach Content-Type if it is not specified at this point
    if (headers["Content-Type"] === undefined) {
      headers["Content-Type"] = awsSigV4Client.defaultContentType;
    }

    return {
      headers: headers,
      url: url
    };
  };

  return awsSigV4Client;
};


// import sigV4Client from "./sigV4Client";
// import * as sigV4Client from './sigV4Client.js'

// console.log('sigV4Client', sigV4Client)

// const apiHost = process.env.API_HOST;
// const IDENTITY_POOL_ID = process.env.INDENTITY_POOL_ID;
// const region = process.env.API_REGION;

AWS.config.region = 'us-east-2';

// Configure the credentials provider to use the user identity pool
AWS.config.credentials = new AWS.CognitoIdentityCredentials({
    IdentityPoolId: "us-east-2:8cbe849d-7449-4e9e-a364-92a123bba715"
});



const getCredential = function getCredential() {
    return new Promise((resolve, reject) => {
        AWS.config.credentials.get(function(err){
            if (err) {
                console.error('err', err);
                reject(err);
            } else {
                const credentials = AWS.config.credentials
                const { accessKeyId, secretAccessKey, sessionToken, expireTime } = credentials;
                console.log("access")
                resolve({ accessKeyId, secretAccessKey, sessionToken, expireTime });
            }
        });
    });
};
//module.exports.authenticatedCall = async function authenticatedCall(authStore) {
const apiHost = "bgdn1cvvbh.execute-api.us-east-2.amazonaws.com";
const path = "/test/drug";
//const url ="https://bgdn1cvvbh.execute-api.us-east-2.amazonaws.com/dev/drug/pricing?ndc=00006073531&price-code=A"
//const url ="https://bgdn1cvvbh.execute-api.us-east-2.amazonaws.com/dev/drug/pricing?ndc=00006073531&price-code=A"
//const url = "https://bgdn1cvvbh.execute-api.us-east-2.amazonaws.com/dev/drug?name=acetaminophen&cs-code=NA&dc-code=NA&rtx-code=R&item-status=A&item-status=O";
const drugLookupUrl ="https://bgdn1cvvbh.execute-api.us-east-2.amazonaws.com/test/druglookup?search-name=lipi"
const drugUrl ="https://bgdn1cvvbh.execute-api.us-east-2.amazonaws.com/test/drug?name=Acetaminophen"
async function getDrugList(credentials) {
    const opts = {
        method: "GET",
        // service: "execute-api",
        service: "execute-api",
        region: AWS.config.region,
        path: '/test/druglookup?search-name=lipi',
        host: apiHost,
        headers: { "accept": "application/json, text/plain, */*"},
        drugLookupUrl: drugLookupUrl,
        drugUrl: drugUrl
    };
    if (!credentials) {
        credentials = await getCredential();
    }

    console.log('credentials', credentials)
    const { accessKeyId, secretAccessKey, sessionToken } = credentials;
    // const accessKeyId = "ASIAYXKB4ISFOE3VS4XF";
    // const secretAccessKey= "dh/LJmab/deo0KdJT+lIOuN5xok96TdZ5cQu9kl4";
    // const sessionToken = "IQoJb3JpZ2luX2VjEJv//////////wEaCXVzLWVhc3QtMiJHMEUCIHnwZSi9HKlrWJAJaIz0PdXMmg1B0DNcZIX6tlcKjpLAAiEA+bgVjujHrrExWixHI42Dmoq/3KV2Szeg7wF2Mxp4j6YqmgYI9P//////////ARAAGgw1OTk4MjI5NzYxMzgiDOWL9JM/P2HQI56RXiruBeejmCX5w+XZp5hTNjmERfIQ6Yyr5vBHugHWS5RZUJf+0CxkBiqNF7Az2APl8CW5uoJh6u0wyzS+v6FvqTkmziWKPBlmLK9K3IKJ/5KfGqy7kj75xlYNzDzMSVBeR3D7zkHbcZ9pXUem0rw8GWD9MwOr1YT19Iq2SAZ4FCRIoXL3Ey04aSDXkDA866QbtdcsBGLxN7WkxRPx/HqsSsY4JaLyL8omffSPALUbU7IT8H+QTp2ndoJNL6goSlkfZMgZV1VPfq0sIZtrQ7qrIvsYYAeWb57G3MJQJFQX6Ryo6NWgpApTS/M1h4kZtI0C6gNcveDeXbScrkOCDWS2NDp56/59SpV7ART9nNc8jHg/ONUBgvfWvJv8yjMZT5nWfffMK1SG9641OOoo0dk3foJnLCqrlekq+7QnlKFSCmsOZhgJu+tKBpi+DY7/irTkAXrte6SO3Xea1q31oRH7+zspfXCfl6e52/Bhx1BLRuAFFF04LnnkYPVxi5OebJBcsdko7d69HmONkmX3ZwL5eFhHiF+bHnxStFnU/xHZT/OTBAQVfKQvyJ/rMrf1u7cO8N/chQGZDxhELbHjW9GracEry25Wkrhs4cIgQN+GXH7SXJO1IpaRkE7DtW3qPaoa3VrkaD5SQMYHf1lqOfQSE+cLXpM8Pv5bMBIParAax+eOMQ4Zt1";
    // console.log("accessKeyId :" + accessKeyId);
    // console.log("secretAccessKey :" + secretAccessKey);
    // console.log("sessionToken:" + sessionToken);
    const request = aws4.sign(opts, {
        accessKeyId,
        secretAccessKey,
        sessionToken
    });

    delete request.headers.Host;
    delete request.headers['Content-Length'];

    console.log('request', request)
    console.log('request.headers', request.headers)


    const signedRequest = sigV4Client
    .newClient({
      accessKey: accessKeyId,
      secretKey: secretAccessKey,
      sessionToken: sessionToken,
      region: AWS.config.region,
      endpoint: 'https://bgdn1cvvbh.execute-api.us-east-2.amazonaws.com'
    })
    .signRequest({
      method: 'GET',
      path: '/test/druglookup',
      headers: {},
      queryParams: {
          'search-name': 'lipi'
      },
      body: {}
    });

    console.log('signedRequest', signedRequest)


    
    const drugListResp = await fetch(opts.drugLookupUrl, {
        headers: request.headers,
        // headers: {
        //     "x-amz-security-token": request.headers['X-Amz-Security-Token'],
        //     "x-amz-date": request.headers['X-Amz-Date'],
        //     "authorization": request.headers['Authorization'],
        // }
        // headers: myOpts.request.header
    //     headers: {

    //         "x-amz-security-token": "IQoJb3JpZ2luX2VjEJ7//////////wEaCXVzLWVhc3QtMiJHMEUCIQDp0orp8P70k8ELTGeu1Pv1ySSX3Gm5P89JZibtXE4lXAIgchQ4bomhH575v22mC0t80H8v2a41Hp6HoGKNzJAi9QIqmgYI9///////////ARAAGgw1OTk4MjI5NzYxMzgiDA2wKZq89iQh9a5M0SruBcCpx8NUHeyn1+mlWKuEZhz3qOibWoedf/uiHuuKOyLgfLEGHkI8oxwIA5Jig7BNgAtBJ/W7EDZsn4Cv9C/WQ9lY6ONlIFWx7OrOcEjXO7RFVvb/O0TAeyKJA2mttVgTZNjcIwgDXzAYJn0TWBJh9xMm5zDECHHxqrI2pACTB/qV1xtvqruC5KAEeRubG4CZGM3On+ozWHf6Ey2IWkKqrF1SLe0T5hbuCyukJxA9YRTXRBvp0jFqCBj4HJcdY4cJj3Hs29aJMfjwXp8dvRwnBzongkDesXCXB+OEhwI8jueka2nYhradTdv1ONNwlebE+6NTtSCw131WkkcuKAb4iHyDVgl5AYQc3af9Q0Hi/+W8lsb0A1sV/jjyTDC0xguYMLRKo4RYBd/JXMGKMi52etl7P/Oc8me/JpuUcRvFYQyGCOOaEQHTWCvGkUy30KFENCuPd3aGtEDijRdPxWnnv0Lpeuzz8nSM5Yhas4TYP2okXaJZIPLxHE9WfzRcOJl+9xxtaE3h81OSSgMMSe2Imhv/LozvXBL4Mxhw1ISyT7mbAcGWDNFW2sijJP2k+tZAW8SNXCaBi2dLXBZsUpFnpv+LKlLoG1ne3ezDl7a/Z0LUHDaCQw0yuwuOkfIhvY7MVvssm5vBIUX2frE8IuhtRh1gv5AobI6KCTbL3B9MZAj0dR9tv+4o/fZLxUYkjJj0Ruo0CQ0mz8MyASXlXD2Oy2uUVG/PQOE1jdCcZQjRlix6hGjgxufRhKmB5tLg0nHmvBUrx15RHn7JW2IBb0RSTVRsIlV8Vq9Xl5mOOlaxMrBijwaegvoO+eBb8T18m1s8Drxi/saCMYvhQb7vBF4qkmwGejh46Xe54csMCCZCPnfaN8TUfjizAfUC61iy/zfr76FNmfPTh8Q5hxA+GnttCW8V8YacUQvAlsco/xJvwF6IZEBXHX0m0J1cTWgjmFNYT/hJ9B3RJVYyaMdlIF1djY1kmBlE0fF6Jq4HNNpvbjCCpKmKBjqHAsuZu7lNas6aYx0UwoMIRluTPT60HbHF4NzgIzyhbq5BoE7uvtqBHeWMN0lTDw+TcTvR960dD3vDq0IPtNY0/mRSGIXLbt5iC1jyJK38bGzioWr/728CdrH3Zgy87qXNNRnMGl4kuomBfe2ecOWK/iZ9jem1C3uhjyXshdE9uei1E5SeciJtqkJmsbE2Z5sHO4sMYk6/XjnaAzh91mJaquY70o6Dy2jZqe6THGWTSX5biSUW2euveUqdK7yZyO4iM9dNjyUCmn+zRkSx2JId8KQfGUWHH27ywq7Tv+7ABx7Igbc2p5VVcPSHwDmS0+eu1y0bVW5Bt6X8DHjpc8GHwiqjiy8a0gAX",
    // "x-amz-date": "20210921T215216Z",
    // "authorization": "AWS4-HMAC-SHA256 Credential=ASIAYXKB4ISFPP7WVWEP/20210921/us-east-2/execute-api/aws4_request, SignedHeaders=host;x-amz-date;x-amz-security-token, Signature=c1baf80f20db274e9fb0aa4c26d3ca2f777711a211d9ca5d1430938a3721d4e0",
    //     }
    });

    console.log("drugListResp:", drugListResp);
    console.log("drugListResp.headers:", drugListResp.headers);
    
    console.log("drugList json:", await drugListResp.json());
    if (drugListResp.ok) {
        const drugList = await drugListResp.json();
        console.log(drugList);
        const drugResp = await fetch(opts.drugUrl, {
            headers: request.headers
        });
        if(drugResp.ok) {
            const drug = await drugResp.json();
            console.log(drug);
            return drug;
        } else {
            return { message: drugResp.statusText };
        }
    } else {
        
        return { message: drugListResp.statusText };
    }
}

const testCred = (async () => {
    try {
        console.log ("====JJ1====");
        const result = await getCredential();
        console.log(result);
        console.log ("====JJ2====");
    } catch (err) {
        console.log(err)
    }
});
const testDrug = (async () => {
    try {
        console.log ("====JJ1====");
        //const cred = await getCredential();
        //console.log(cred)
        const drugList = await getDrugList(null);
        console.log(JSON.stringify(drugList));
        console.log ("====JJ2====");
    } catch (err) {
        console.log(err)
    }
});

testDrug();




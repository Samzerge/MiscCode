'use strict';
const AWS = require("aws-sdk");
const aws4  = require('aws4');
const fetch = require("node-fetch");

// const apiHost = process.env.API_HOST;
// const IDENTITY_POOL_ID = process.env.INDENTITY_POOL_ID;
// const region = process.env.API_REGION;

AWS.config.region = 'us-east-2';

// Configure the credentials provider to use the user identity pool
AWS.config.credentials = new AWS.CognitoIdentityCredentials({
    IdentityPoolId: "us-east-2:8cbe849d-7449-4e9e-a364-92a123bba715"
});

const getCredential = function getCredential() {
    return new Promise((resolve, reject) => {
        AWS.config.credentials.get(function(err){
            if (err) {
                console.error('err', err);
                reject(err);
            } else {
                const credentials = AWS.config.credentials
                const { accessKeyId, secretAccessKey, sessionToken, expireTime } = credentials;
                console.log("access")
                resolve({ accessKeyId, secretAccessKey, sessionToken, expireTime });
            }
        });
    });
};
//module.exports.authenticatedCall = async function authenticatedCall(authStore) {
const apiHost = "bgdn1cvvbh.execute-api.us-east-2.amazonaws.com";
const path = "/dev/drug";
//const url ="https://bgdn1cvvbh.execute-api.us-east-2.amazonaws.com/dev/drug/pricing?ndc=00006073531&price-code=A"
//const url ="https://bgdn1cvvbh.execute-api.us-east-2.amazonaws.com/dev/drug/pricing?ndc=00006073531&price-code=A"
//const url = "https://bgdn1cvvbh.execute-api.us-east-2.amazonaws.com/dev/drug?name=acetaminophen&cs-code=NA&dc-code=NA&rtx-code=R&item-status=A&item-status=O";
const drugLookupUrl ="https://bgdn1cvvbh.execute-api.us-east-2.amazonaws.com/dev/druglookup?search-name=Acet"
const drugUrl ="https://bgdn1cvvbh.execute-api.us-east-2.amazonaws.com/dev/drug?name=Acetaminophen"
async function getDrugList(credentials) {
    const opts = {
        method: "GET",
        service: "execute-api",
        region: AWS.config.region,
        path: path,
        host: apiHost,
        headers: { "accept": "application/json, text/plain, */*"},
        drugLookupUrl: drugLookupUrl,
        drugUrl: drugUrl
    };
    if (!credentials) {
        credentials = await getCredential();
    }
    const { accessKeyId, secretAccessKey, sessionToken } = credentials;
    console.log("accessKeyId 2:" + accessKeyId);
    const request = aws4.sign(opts, {
        accessKeyId,
        secretAccessKey,
        sessionToken
    });
    delete request.headers.Host;
    const drugListResp = await fetch(opts.drugLookupUrl, {
        headers: request.headers
    });
    console.log("drugListResp:", drugListResp);
    if (drugListResp.ok) {
        const drugList = await drugListResp.json();
        console.log(drugList);
        const drugResp = await fetch(opts.drugUrl, {
            headers: request.headers
        });
        if(drugResp.ok) {
            const drug = await drugResp.json();
            console.log(drug);
            return drug;
        } else {
            return { message: drugResp.statusText };
        }
    } else {
        console.log("drugList json:", await drugListResp.json());
        return { message: drugListResp.statusText };
    }
}

const testCred = (async () => {
    try {
        console.log ("====JJ1====");
        const result = await getCredential();
        console.log(result);
        console.log ("====JJ2====");
    } catch (err) {
        console.log(err)
    }
});
const testDrug = (async () => {
    try {
        console.log ("====JJ1====");
        //const cred = await getCredential();
        //console.log(cred)
        const drugList = await getDrugList(null);
        console.log(JSON.stringify(drugList));
        console.log ("====JJ2====");
    } catch (err) {
        console.log(err)
    }
});

testDrug();



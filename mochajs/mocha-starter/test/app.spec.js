const assert = require('assert')
const {add} = require('../src/app')

describe('the add functon', () => {
    it ('should add 2 numbers together', () => {
        const result = add(2,2)

        assert.equal(result, 4)
    })
})
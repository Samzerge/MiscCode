import * as cdk from '@aws-cdk/core';
import * as cognito from "@aws-cdk/aws-cognito";
import * as iam from "@aws-cdk/aws-iam";
import { CfnOutput, Duration } from '@aws-cdk/core';
import { UserPool } from '@aws-cdk/aws-cognito';
import { PolicyStatement } from '@aws-cdk/aws-iam';
// import * as sst from "@serverless-stack/resources";

export class CognitoConstruct extends cdk.Construct {
    public userPool: UserPool
    constructor(scope: cdk.Construct, id: string, props?: cdk.StackProps) {
        super(scope, id);

        // const userPool = new cognito.UserPool(this, 'UserPool', {
        //     selfSignUpEnabled: false,
        //     signInAliases: {
        //         username: false,
        //         email: true,
        //         phone: false,
        //         preferredUsername: false
        //     },
        //     autoVerify: {
        //         email: false,
        //         phone: false
        //     },
        //     accountRecovery: cognito.AccountRecovery.NONE,
        //     passwordPolicy: {
        //         minLength: 8,
        //         requireUppercase: false,
        //         requireLowercase: false,
        //         requireDigits: false,
        //         requireSymbols: false
        //     }
        // });
    
        
    
        // const userPoolClient = userPool.addClient('AppClient', {
        //     idTokenValidity: Duration.minutes(5),
        //     accessTokenValidity: Duration.minutes(60),
        //     refreshTokenValidity: Duration.days(30),
        //     supportedIdentityProviders: [
        //         cognito.UserPoolClientIdentityProvider.COGNITO
        //     ]
        // })
        // const clientId = userPoolClient.userPoolClientId
    
        const identityPool = new cognito.CfnIdentityPool(this, 'IdentityPool', {
            allowUnauthenticatedIdentities: true,
            cognitoIdentityProviders: [
            ],
        })

        // const aRole = {
        //     "Version": "2012-10-17",
        //     "Statement": [
        //       {
        //         "Effect": "Allow",
        //         "Action": [
        //           "mobileanalytics:PutEvents",
        //           "cognito-sync:*",
        //           "cognito-identity:*"
        //         ],
        //         "Resource": [
        //           "*"
        //         ]
        //       }
        //     ]
        //   }

        // const uRole = {
        //     "Version": "2012-10-17",
        //     "Statement": [
        //       {
        //         "Effect": "Allow",
        //         "Action": [
        //           "mobileanalytics:PutEvents",
        //           "cognito-sync:*"
        //         ],
        //         "Resource": [
        //           "*"
        //         ]
        //       }
        //     ]
        //   }

        const authRole = new iam.Role(this, 'CognitoAuthRole', {
            description: 'Default role for authenticated users',
            assumedBy: new iam.FederatedPrincipal(
                'cognito-identity.amazonaws.com',
                {
                    StringEquals: {
                        'cognito-identity.amazonaws.com:aud': identityPool.ref,
                    },
                    'ForAnyValue:StringLike': {
                        'cognito-identity.amazonaws.com:amr': 'authenticated',
                    },
                },
                'sts:AssumeRoleWithWebIdentity',
            ),
            managedPolicies: [
                iam.ManagedPolicy.fromAwsManagedPolicyName(
                    'service-role/AWSLambdaBasicExecutionRole',
                ),
            ],
        });

        const authPolicy = new PolicyStatement({
            effect: iam.Effect.ALLOW,
            actions: [
                "mobileanalytics:PutEvents",
                "cognito-sync:*",
                "cognito-identity:*",
                "execute-api:Invoke"
            ],
            resources: [
                "*"
            ]
        })

        authRole.addToPolicy(authPolicy)

        const unAuthRole = new iam.Role(this, 'CognitoUnauthRole', {
            description: 'Default role for anonymous users',
            assumedBy: new iam.FederatedPrincipal(
                'cognito-identity.amazonaws.com',
                {
                    StringEquals: {
                        'cognito-identity.amazonaws.com:aud': identityPool.ref,
                    },
                    'ForAnyValue:StringLike': {
                        'cognito-identity.amazonaws.com:amr': 'authenticated',
                    },
                },
                'sts:AssumeRoleWithWebIdentity',
            ),
            managedPolicies: [
                iam.ManagedPolicy.fromAwsManagedPolicyName(
                    'service-role/AWSLambdaBasicExecutionRole',
                ),
            ],
        })

        const unAuthPolicy = new PolicyStatement(
            // {
            //     effect: iam.Effect.ALLOW,
            //     actions: [
            //         "mobileanalytics:PutEvents",
            //         "cognito-sync:*",
            //     ],
            //     resources: [
            //         "*"
            //     ]
            // },
            // {
            //     effect: iam.Effect.ALLOW,
            //     actions: [
            //         "execute-api:Invoke"
            //     ],
            //     resources: [
            //         "execute-api:/prod/*"
            //     ]
            // }
        )

        unAuthRole.addToPolicy(unAuthPolicy)
        // unAuthRole.addToPolicy({
        //     effect: iam.Effect.ALLOW,
        //     actions : [
        //         "execute-api:Invoke"
        //     ],
        //     resources : [
        //         "arn:aws:execute-api:us-east-1:123412341234:abcde12345/*/GET/cip",
        //         "arn:aws:execute-api:us-east-1:123412341234:abcde12345/*/POST/cip"
        //     ]
        // })
                
        const identityPoolRoleAttachment = new cognito.CfnIdentityPoolRoleAttachment(this, 'IdentityPoolRoleAttachment', {
            identityPoolId: identityPool.ref,
            roles: {
                authenticated: authRole.roleArn,
                unauthenticated: unAuthRole.roleArn,
            }
        })
    
        
    
        // const testUser = new cognito.CfnUserPoolUser(this, 'TestUser', {
        //     username: 'kaitensoft18@gmail.com',
        //     userPoolId: userPool.userPoolId,
        // })

        // this.userPool = userPool

        // Export values
        // new CfnOutput(this, "UserPoolId", {
        //     value: userPool.userPoolId,
        // });
        // new CfnOutput(this, "UserPoolClientId", {
        //     value: userPoolClient.userPoolClientId,
        // });
        // new CfnOutput(this, "IdentityPoolId", {
        //     value: identityPool.ref,
        // });
    }

}
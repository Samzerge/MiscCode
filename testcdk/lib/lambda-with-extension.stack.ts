import * as core from "@aws-cdk/core";
import * as cdk from '@aws-cdk/core';
import * as cognito from "@aws-cdk/aws-cognito";
import * as iam from "@aws-cdk/aws-iam";
import * as lambda from "@aws-cdk/aws-lambda";
import * as events from "@aws-cdk/aws-events";
import { CfnOutput, Duration, RemovalPolicy } from '@aws-cdk/core';
import { UserPool } from '@aws-cdk/aws-cognito';
import { PolicyStatement } from '@aws-cdk/aws-iam';
import { Alias, Version } from '@aws-cdk/aws-lambda';

export class LambdaWithExtensionStack extends cdk.Stack {

    functionName = 'lambdaWithExtension'

    constructor(scope: cdk.Construct, id: string, props?: any) {
        super(scope, id)

        const lambdaRuntime = lambda.Runtime.NODEJS_14_X

        
        const layerVersionName = `lambda-extension`
        const codePath = `resources/lambdas/layers/extensions/${layerVersionName}/extension.zip`
        const layer: lambda.LayerVersion = new lambda.LayerVersion(this, "LambdaExrension", {   
            code: lambda.Code.fromAsset(codePath),
            compatibleRuntimes: [lambdaRuntime],
            layerVersionName: layerVersionName,
            removalPolicy: RemovalPolicy.DESTROY
        })

        



        const lambdaRuntimes = lambda.Runtime.NODEJS_14_X
  
          
        const lambdaEnvironmentVariables = {

        }
  
        const lambdaFunction = new lambda.Function(this, "lambdaWithExtension", {
            code: lambda.Code.fromAsset("resources/lambdas/lambdaWithExtension"),
            handler: "lambdaWithExtension.lambdaWithExtension",
            environment: {
              ...lambdaEnvironmentVariables,
            },
            runtime: lambdaRuntimes,
            layers: [
                layer
            ]    
        })


        new cdk.CfnOutput(this, 'LambdaLayerOutput', {
            value: layer.layerVersionArn,
            description: 'The arn of the Lambda Layer',
            exportName: `LambdaLayerVersionARN`,
        })

        new cdk.CfnOutput(this, 'LambdaFunctionOutput', {
            value: lambdaFunction.functionArn,
            description: 'The arn of the Lambda',
            exportName: `LambdaFunctionARN`,
        })
        
    }
}
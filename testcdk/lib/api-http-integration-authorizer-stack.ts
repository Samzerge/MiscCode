import * as cdk from '@aws-cdk/core';
import * as apigateway from "@aws-cdk/aws-apigateway";
import * as cognito from "@aws-cdk/aws-cognito";
import * as iam from "@aws-cdk/aws-iam";
import * as lambda from "@aws-cdk/aws-lambda";
import { LambdaLayer } from './components/lambda-layer';
import { LambdaAuthorizer } from './components/lambda-authorizer';

export class ApiHttpIntegrationAuthorizerStack extends cdk.Stack {
    
    constructor(scope: cdk.Construct, id: string, props?: any) {
      super(scope, id, props)

      const lambdaRuntime = lambda.Runtime.NODEJS_14_X

        const implementationFunction = new lambda.Function(this, "TestAuthorizerFunction", {
            runtime: lambdaRuntime,
            code: lambda.Code.fromAsset("resources/lambdas/authorizer"),
            handler: "authorizer.authorizer",

        })

      const restApi = new apigateway.RestApi(this, "TestApi", {
        description: "This service is the api.",
        })


        const httpIntegration = new apigateway.HttpIntegration('https://swapi.dev/api/people/1/', {
            // requestTemplates: { "application/json": '{ "statusCode": "200" }' },
            proxy: true, 
            httpMethod: 'GET',
            options: {

            }
          });

          const testResourceNone = restApi.root.addResource('test')
          const testMethod = testResourceNone.addMethod('GET', httpIntegration, {
            apiKeyRequired: false,
            // authorizationType: apigateway.AuthorizationType.CUSTOM,
            // authorizer: lambdaAuthorizer.implementationFunction.functionArn
          })
      
    }
}
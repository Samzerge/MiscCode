import { LambdaLayer } from './lambda-layer'
import { TestcdkStack } from './../testcdk-stack';

import * as cdk from '@aws-cdk/core';
import * as cognito from "@aws-cdk/aws-cognito";
import * as iam from "@aws-cdk/aws-iam";
import * as lambda from "@aws-cdk/aws-lambda";
import { CfnOutput, Duration, RemovalPolicy } from '@aws-cdk/core';
import { UserPool } from '@aws-cdk/aws-cognito';
import { PolicyStatement } from '@aws-cdk/aws-iam';
import outputsJson from '../../resources/outputs/outputs.json'

export class LambdaLayerExtension extends cdk.Stack {

    constructor(scope: cdk.Construct, id: string, version: number, props?: cdk.StackProps) {
        super(scope, id)

        const lambdaRuntime = lambda.Runtime.NODEJS_12_X

        
        const layerVersionName = `lambda-extension`
        const codePath = `resources/lambdas/layers/extensions/${layerVersionName}/nodejs.zip`
        const layer: lambda.LayerVersion = new lambda.LayerVersion(this, "VersionedLambdaLayer", {   
            code: lambda.Code.fromAsset(codePath),
            compatibleRuntimes: [lambda.Runtime.NODEJS_14_X],
            layerVersionName: layerVersionName,
            removalPolicy: RemovalPolicy.DESTROY
        })

        
        new cdk.CfnOutput(this, 'LambdaLayerOutput' + version, {
            value: layer.layerVersionArn,
            description: 'The arn of the Lambda Layer with layer stack version:' + version,
            exportName: `LambdaLayerVersion` + version + `ARN`,
        })
    }
}
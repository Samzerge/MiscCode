import * as cdk from "@aws-cdk/core";
import * as apigateway from "@aws-cdk/aws-apigateway";
import * as lambda from "@aws-cdk/aws-lambda";
import * as rds from '@aws-cdk/aws-rds';
import * as ec2 from '@aws-cdk/aws-ec2';
import * as iam from "@aws-cdk/aws-iam";
import { JsonSchemaType, JsonSchemaVersion, RestApi } from "@aws-cdk/aws-apigateway";


export type DrugApiModels = {
    httpErrorModel: apigateway.Model
    databaseErrorModel: apigateway.Model
    drugResponseModel: apigateway.Model
    drugLookupResponseModel: apigateway.Model
    drugDescriptionResponseModel: apigateway.Model
    drugPricingResponseModel: apigateway.Model
    drugPackagingResponseModel: apigateway.Model
    drugImageResponseModel: apigateway.Model
    drugFlagsResponseModel: apigateway.Model
    emptyModel: apigateway.IModel
    errorModel: apigateway.IModel
}

export type DrugMethodResponses = {
    method200SharedResponse: apigateway.MethodResponse
    method400SharedResponse: apigateway.MethodResponse
    method404SharedResponse: apigateway.MethodResponse
    method408SharedResponse: apigateway.MethodResponse
    method500SharedResponse: apigateway.MethodResponse
    method501SharedResponse: apigateway.MethodResponse
}

enum DrugResource {
    DRUG,
    DRUGLOOKUP,
    DRUG_IMAGE,
    DRUG_FLAGS,
    DRUG_PACKAGING,
    DRUG_PRICING,
    DRUG_DESCRIPTION

}
export class DrugApiConstruct extends cdk.Construct {

    restApiId = '7oun6o1r1m'
    rootResourceId = 'h57c6pm2o6'

    accesControlAllowHeaders = 'Content-Type,X-Amz-Date,Authorization,X-Api-Key,X-Amz-Security-Token'
    accessControlAllowMethods = 'GET,OPTIONS'

    testingStage = true

    constructor(scope: cdk.Construct, id: string) {
        super(scope, id)

        const usedAuthorizationType = this.testingStage ? apigateway.AuthorizationType.NONE : apigateway.AuthorizationType.IAM

        const responseHeadersConfig = {
            'Access-Control-Allow-Origin': `'${apigateway.Cors.ALL_ORIGINS.join(',')}'`,
            'Access-Control-Allow-Headers': `'${this.accesControlAllowHeaders}'`,
            'Access-Control-Allow-Methods':  `'${this.accessControlAllowMethods}'`
        }

        const restApi: apigateway.IRestApi = apigateway.RestApi.fromRestApiAttributes(this, 'birdirx-api', {
            restApiId: this.restApiId,
            rootResourceId: this.rootResourceId,
            
        })
        const restApiCasted = restApi as RestApi

        const gatewayResponse400Properties =  {
            type: apigateway.ResponseType.DEFAULT_4XX,
            responseHeaders: responseHeadersConfig
        }
        const gatewayResponse500Properties =  {
            type: apigateway.ResponseType.DEFAULT_5XX,
            responseHeaders: responseHeadersConfig
        }

        restApiCasted.addGatewayResponse('GatewayResponse4XX', gatewayResponse400Properties)
        restApiCasted.addGatewayResponse('GatewayResponse5XX', gatewayResponse500Properties)

        const defaultRequestValidator = new apigateway.RequestValidator(this, 'api-default-request-validator', {
            restApi: restApi,
            validateRequestBody: false,
            validateRequestParameters: true,
        })

        const lambdaIntegrationOptions: apigateway.LambdaIntegrationOptions = {
            proxy: true,
            allowTestInvoke: false,
        }
        
        const responseModelKey = 'application/json'
        const apiModels: DrugApiModels = this.apiModelsSetup(restApi)
        const responseModels: DrugMethodResponses = this.responseModelsSetup(responseModelKey, apiModels)
        
        const sharedMethodsOptions: apigateway.MethodOptions = {
            apiKeyRequired: false,
            methodResponses: [],
            requestValidator:defaultRequestValidator
        }
        
        const sharedResponseModels = [
            responseModels.method400SharedResponse,
            responseModels.method404SharedResponse,
            responseModels.method408SharedResponse,
            responseModels.method500SharedResponse,
            responseModels.method501SharedResponse,
        ]
        
        // const methodOptions = this.methodOptionsSetup(defaultRequestValidator, responseModelKey, responseModels, apiModels)
        const drugResource = this.resourceSetup(DrugResource.DRUG, restApi.root, lambdaIntegrationOptions, sharedMethodsOptions, sharedResponseModels, responseModels, responseModelKey, apiModels,
            usedAuthorizationType)
        this.resourceSetup(DrugResource.DRUGLOOKUP, restApi.root, lambdaIntegrationOptions, sharedMethodsOptions, sharedResponseModels, responseModels, responseModelKey, apiModels,
            usedAuthorizationType)




        this.resourceSetup(DrugResource.DRUG_DESCRIPTION, drugResource, lambdaIntegrationOptions, sharedMethodsOptions, sharedResponseModels, responseModels, responseModelKey, apiModels,
            usedAuthorizationType)
        this.resourceSetup(DrugResource.DRUG_PRICING, drugResource, lambdaIntegrationOptions, sharedMethodsOptions, sharedResponseModels, responseModels, responseModelKey, apiModels,
            usedAuthorizationType)
        this.resourceSetup(DrugResource.DRUG_PACKAGING, drugResource, lambdaIntegrationOptions, sharedMethodsOptions, sharedResponseModels, responseModels, responseModelKey, apiModels,
            usedAuthorizationType)
        this.resourceSetup(DrugResource.DRUG_IMAGE, drugResource, lambdaIntegrationOptions, sharedMethodsOptions, sharedResponseModels, responseModels, responseModelKey, apiModels,
            usedAuthorizationType)
        this.resourceSetup(DrugResource.DRUG_FLAGS, drugResource, lambdaIntegrationOptions, sharedMethodsOptions, sharedResponseModels, responseModels, responseModelKey, apiModels,
            usedAuthorizationType)



        // README!!!!!!
        /**
         * 
         * Steps for deployment to work correctly:
         * 
         * 1. Make the changes to the API
         * 2. Deploy changes (DO NOT change any api deployment yet)
         * 3. Once api changes have been deployed update apigateway.Deployments
         * 4. Create new deployment object with new changes
         * 5. Keep the previous deployment object unchanged (specially the name)
         */


        /**
         * Deplyment 1 contains basic features
         */
        const deployment1 = new apigateway.Deployment(this, 'CDK Api Deployment 1', {
            api: restApi,
            description: 'Test 1',
            retainDeployments: true
            
        })
        const deployment2 = new apigateway.Deployment(this, 'CDK Api Deployment 2', {
            api: restApi,
            description: 'Test 2',
            retainDeployments: true
            
        })
    
        const testStage = new apigateway.Stage(this, 'Test Stage', {
            deployment: deployment2,
            stageName: 'changedTest'
        })
        const devStage = new apigateway.Stage(this, 'Dev Stage', {
            deployment: deployment1,
            stageName: 'dev'
        })
    }

    apiModelsSetup = (restApi: apigateway.IRestApi) => {

        const drugInfoResponseModelTitle = 'DrugResponseModel'
        const drugLookupResponseModelTitle = 'DrugLookupResponseModel'
        const drugDescriptionResponseModelTitle = 'DrugDescriptionResponseModel'
        
        const drugPricingModelTitle = 'DrugPricingResponseModel'
        const drugPackagingModelTitle = 'DrugPackagingResponseModel'
        const drugImageModelTitle = 'DrugImageResponseModel'
        const drugFlagsModelTitle = 'DrugFlagsResponseModel'

        const httpErrorModelTitle = 'HttpErrorModelOptionModel'
        const databaseErrorModelTitle = 'DatabaseErrorModel'

        const sharedSchemaProperties: apigateway.JsonSchema = {
            schema: JsonSchemaVersion.DRAFT4,
        }
        const sharedModelProperties: apigateway.ModelProps = {
            restApi: restApi,
            contentType: 'application/json',
            schema: {
                ...sharedSchemaProperties
            }
        }

        const httpErrorModel = new apigateway.Model(this, httpErrorModelTitle, {
            ...sharedModelProperties,
            modelName: httpErrorModelTitle,
            description: 'HTTP error code from Lambda',
            schema: {
                title: httpErrorModelTitle,
                schema: JsonSchemaVersion.DRAFT4,
                type: JsonSchemaType.OBJECT,
                properties: {
                    'code': {
                        type: JsonSchemaType.NUMBER,
                    },
                    'status': {
                        type: JsonSchemaType.STRING,
                    },
                    'message': {
                        type: JsonSchemaType.STRING,
                    }
                }
            }
        })

        const databaseErrorModel = new apigateway.Model(this, databaseErrorModelTitle, {
            ...sharedModelProperties,
            modelName: databaseErrorModelTitle,
            description: 'The error format returned from the database',
            schema: {
                title: databaseErrorModelTitle,
                schema: JsonSchemaVersion.DRAFT4,
                type: JsonSchemaType.OBJECT,
                properties: {
                    'code': {
                        type: JsonSchemaType.STRING,
                    },
                    'errno': {
                        type: JsonSchemaType.NUMBER,
                    },
                    'sqlMessage': {
                        type: JsonSchemaType.STRING,
                    },
                    'sqlState': {
                        type: JsonSchemaType.STRING,
                    },
                    'sql': {
                        type: JsonSchemaType.STRING,
                    },
                }
            }
        })

        /**
         * Example:
         * {"drugName":"Zocor","dosageForms":[{"dosageForm":"Tablet","strengths":[{"strength":"10","strengthUnit":"MG","fullStrength":"10 MG","ddid":"024224","gpi":"39400075000320","formCode":"F109","formDescription":"Solid","routeAdmin":"Oral"},{"strength":"20","strengthUnit":"MG","fullStrength":"20 MG","ddid":"024225","gpi":"39400075000330","formCode":"F109","formDescription":"Solid","routeAdmin":"Oral"},{"strength":"40","strengthUnit":"MG","fullStrength":"40 MG","ddid":"024226","gpi":"39400075000340","formCode":"F109","formDescription":"Solid","routeAdmin":"Oral"},{"strength":"5","strengthUnit":"MG","fullStrength":"5 MG","ddid":"024227","gpi":"39400075000310","formCode":"F109","formDescription":"Solid","routeAdmin":"Oral"},{"strength":"80","strengthUnit":"MG","fullStrength":"80 MG","ddid":"056711","gpi":"39400075000360","formCode":"F109","formDescription":"Solid","routeAdmin":"Oral"}]}]}
         */
        
        const drugInfoModel = new apigateway.Model(this, drugInfoResponseModelTitle, {
            ...sharedModelProperties,
            modelName: drugInfoResponseModelTitle,
            description: 'Drug with dosage forms and strengths',
            schema: {
                title: drugInfoResponseModelTitle,
                schema: JsonSchemaVersion.DRAFT4,
                type: JsonSchemaType.OBJECT,
                properties: {
                    'drugName': {
                        type: JsonSchemaType.STRING,
                    },
                    'dosageForms': {
                        type: JsonSchemaType.ARRAY,
                        items: {
                            type: JsonSchemaType.OBJECT,
                            properties: {
                                'dosageForm': {
                                    type: JsonSchemaType.STRING,
                                },
                                'strengths': {
                                    type: JsonSchemaType.ARRAY,
                                    items: {
                                        type: JsonSchemaType.OBJECT,
                                        properties: {
                                            'strength': {
                                                type: JsonSchemaType.STRING
                                            },
                                            'strengthUnit': {
                                                type: JsonSchemaType.STRING
                                            },
                                            'fullStrength': {
                                                type: JsonSchemaType.STRING
                                            },
                                            'ddid': {
                                                type: JsonSchemaType.STRING
                                            },
                                            'gpi': {
                                                type: JsonSchemaType.STRING
                                            },
                                            'formCode': {
                                                type: JsonSchemaType.STRING
                                            },
                                            'formDescription': {
                                                type: JsonSchemaType.STRING
                                            },
                                            'routeAdmin': {
                                                type: JsonSchemaType.STRING
                                            },
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        })

        /**
         * Example:
         * [{"drugName":"Zocor","deaClassCode":"NA","deaClassCodeDesc":"DEA Class Code is not applicable"},{"drugName":"CeQur Simplicity 2U","deaClassCode":"NA","deaClassCodeDesc":"DEA Class Code is not applicable"},{"drugName":"CeQur Simplicity Inserter","deaClassCode":"NA","deaClassCodeDesc":"DEA Class Code is not applicable"},{"drugName":"CeQur Simplicity Starter","deaClassCode":"NA","deaClassCodeDesc":"DEA Class Code is not applicable"},{"drugName":"Sacro Brace","deaClassCode":"NA","deaClassCodeDesc":"DEA Class Code is not applicable"},{"drugName":"Sacro Brace/Thermo-Pad","deaClassCode":"NA","deaClassCodeDesc":"DEA Class Code is not applicable"},{"drugName":"Sacro-Cushion","deaClassCode":"NA","deaClassCodeDesc":"DEA Class Code is not applicable"},{"drugName":"Scar","deaClassCode":"NA","deaClassCodeDesc":"DEA Class Code is not applicable"},{"drugName":"Scar Care","deaClassCode":"NA","deaClassCodeDesc":"DEA Class Code is not applicable"},{"drugName":"Scar Care Base Enhanced","deaClassCode":"NA","deaClassCodeDesc":"DEA Class Code is not applicable"}]
         */
        const drugLookupResponseModel = new apigateway.Model(this, drugLookupResponseModelTitle, {
            ...sharedModelProperties,
            modelName: drugLookupResponseModelTitle,
            description: 'Drug names list from search',
            schema: {
                title: drugLookupResponseModelTitle,
                schema: JsonSchemaVersion.DRAFT4,
                type: JsonSchemaType.ARRAY,
                items: {
                    type: JsonSchemaType.OBJECT,
                    properties: {
                        'drugName': {
                            type: JsonSchemaType.STRING,
                        },
                        'deaClassCode': {
                            type: JsonSchemaType.STRING,
                        },
                        'deaClassCodeDesc': {
                            type: JsonSchemaType.STRING,
                        },
                    }
                }
            }
        })

        
        // TODO: property to camel case
        const drugDescriptionModel = new apigateway.Model(this, drugDescriptionResponseModelTitle, {
            ...sharedModelProperties,
            modelName: drugDescriptionResponseModelTitle,
            description: 'HTML leaflet for a specific drug',
            schema: {
                title: drugDescriptionResponseModelTitle,
                schema: JsonSchemaVersion.DRAFT4,
                type: JsonSchemaType.OBJECT,
                properties: {
                    'html_desc': {
                        type: JsonSchemaType.STRING,
                    }
                }
            }
        })

        /**
         * Example:
         * {"ndc":"00006073531","priceCode":"A","priceEffectiveDate":"20170608","unitPrice":5.544,"unitPriceExtended":5.544,"packagePrice":166.32,"awpIndicatorCode":"L"}
         */
        const drugPricingModel = new apigateway.Model(this, drugPricingModelTitle, {
            ...sharedModelProperties,
            modelName: drugPricingModelTitle,
            description: 'Pricing data for a specific drug',
            schema: {
                title: drugPricingModelTitle,
                schema: JsonSchemaVersion.DRAFT4,
                type: JsonSchemaType.OBJECT,
                properties: {
                    'ndc': {
                        type: JsonSchemaType.STRING,
                    },
                    'priceCode': {
                        type: JsonSchemaType.STRING,
                    },
                    'priceEffectiveDate': {
                        type: JsonSchemaType.STRING,
                    },
                    'unitPrice': {
                        type: JsonSchemaType.STRING,
                    },
                    'unitPriceExtended': {
                        type: JsonSchemaType.STRING,
                    },
                    'packagePrice': {
                        type: JsonSchemaType.STRING,
                    },
                    'awpIndicatorCode': {
                        type: JsonSchemaType.STRING,
                    },
                }
            }
        })

        /**
         * Example:
         * {"ndc":"00006073531","ddi":"024224","gpi":"39400075000320","gppc":"13598040","gppcCore":"13598","gppcSuffix":"040","packageSize":30,"packageSizeUom":"Each","packageQuantity":1,"totalPackageQuantity":30,"packageDescription":"Bottle","unitDoseUnitUsePkg":"Unit-of-Use Packaging","unbreakable":1}
         */

         const drugPackagingModel = new apigateway.Model(this, drugPackagingModelTitle, {
            ...sharedModelProperties,
            modelName: drugPackagingModelTitle,
            description: 'Packaging data for a specific drug',
            schema: {
                title: drugPackagingModelTitle,
                schema: JsonSchemaVersion.DRAFT4,
                type: JsonSchemaType.OBJECT,
                properties: {
                    'ndc': {
                        type: JsonSchemaType.STRING,
                    },
                    'ddi': {
                        type: JsonSchemaType.STRING,
                    },
                    'gpi': {
                        type: JsonSchemaType.STRING,
                    },
                    'gppc': {
                        type: JsonSchemaType.STRING,
                    },
                    'gppcCore': {
                        type: JsonSchemaType.STRING,
                    },
                    'gppcSuffix': {
                        type: JsonSchemaType.STRING,
                    },
                    'packageSize': {
                        type: JsonSchemaType.NUMBER,
                    },
                    'packageSizeUom': {
                        type: JsonSchemaType.STRING,
                    },
                    'packageQuantity': {
                        type: JsonSchemaType.NUMBER,
                    },
                    'totalPackageQuantity': {
                        type: JsonSchemaType.NUMBER,
                    },
                    'packageDescription': {
                        type: JsonSchemaType.STRING,
                    },
                    'unitDoseUnitUsePkg': {
                        type: JsonSchemaType.STRING,
                    },
                    'unbreakable': {
                        type: JsonSchemaType.NUMBER,
                    },
                }
            }
        })
        const drugImageModel = new apigateway.Model(this, drugImageModelTitle, {
            ...sharedModelProperties,
            modelName: drugImageModelTitle,
            description: 'Image for a specific drug',
            schema: {
                title: drugImageModelTitle,
                schema: JsonSchemaVersion.DRAFT4,
                type: JsonSchemaType.OBJECT,
                properties: {
                    'url': {
                        type: JsonSchemaType.STRING,
                    }
                }
            }
        })
        const drugFlagsModel = new apigateway.Model(this, drugFlagsModelTitle, {
            ...sharedModelProperties,
            modelName: drugFlagsModelTitle,
            description: 'Flags for a specific drug',
            schema: {
                title: drugFlagsModelTitle,
                schema: JsonSchemaVersion.DRAFT4,
                type: JsonSchemaType.OBJECT,
                properties: {
                    'name.drugName': {
                        type: JsonSchemaType.STRING,
                    },
                    'ndc.deaClassCode': {
                        type: JsonSchemaType.STRING,
                    },
                    'name.controlledSubstanceCode': {
                        type: JsonSchemaType.STRING,
                    },
                    'ndc.itemStatusFlag': {
                        type: JsonSchemaType.STRING,
                    },
                    'ndc.rxOtcIndicatorCode': {
                        type: JsonSchemaType.STRING,
                    },
                    'name.methaphone': {
                        type: JsonSchemaType.STRING,
                    },
                    'name.soundsLike': {
                        type: JsonSchemaType.STRING,
                    },
                }
            }
        })
        

        const emptyModel = apigateway.Model.fromModelName(this, 'EmptyModel', 'Empty')
        const errorModel = apigateway.Model.fromModelName(this, 'ErrorModel', 'Error')

        return {
            httpErrorModel,
            databaseErrorModel,
            drugResponseModel: drugInfoModel,
            drugLookupResponseModel,
            drugDescriptionResponseModel: drugDescriptionModel,
            drugPricingResponseModel: drugPricingModel,
            drugPackagingResponseModel: drugPackagingModel,
            drugImageResponseModel: drugImageModel,
            drugFlagsResponseModel: drugFlagsModel,
            emptyModel,
            errorModel
        }
    }

    responseModelsSetup = (responseModelKey: string, apiModels: DrugApiModels) => {

        const sharedResponseParameters = {
            // 'method.response.header.Access-Control-Allow-Origin': true,
        }
        const method200SharedResponse: apigateway.MethodResponse = {

            statusCode: '200',
            responseParameters: {
                ...sharedResponseParameters
            },
        }
        const method400SharedResponse: apigateway.MethodResponse = {
            
            statusCode: '400',
            responseParameters: {
                ...sharedResponseParameters
            },
            responseModels: {
                [responseModelKey]: apiModels.httpErrorModel
            }
        }
        const method404SharedResponse: apigateway.MethodResponse = {
            
            statusCode: '404',
            responseParameters: {
                ...sharedResponseParameters
            },
            responseModels: {
                [responseModelKey]: apiModels.httpErrorModel
            }
        }
        const method408SharedResponse: apigateway.MethodResponse = {
            
            statusCode: '408',
            responseParameters: {
                ...sharedResponseParameters
            },
            responseModels: {
                [responseModelKey]: apiModels.httpErrorModel
            }
        }
        const method500SharedResponse: apigateway.MethodResponse = {

            statusCode: '500',
            responseParameters: {
                ...sharedResponseParameters
            },
            responseModels: {
                [responseModelKey]: apiModels.databaseErrorModel
            }
        }
        const method501SharedResponse: apigateway.MethodResponse = {

            statusCode: '501',
            responseParameters: {
                ...sharedResponseParameters
            },
            responseModels: {
                [responseModelKey]: apiModels.httpErrorModel
            }
        }
        return {
            method200SharedResponse,
            method400SharedResponse,
            method404SharedResponse,
            method408SharedResponse,
            method500SharedResponse,
            method501SharedResponse,
        }
    }

    resourceSetup = (drugResource: DrugResource, parentResource: apigateway.IResource, lambdaIntegrationOptions: apigateway.LambdaIntegrationOptions,
        sharedMethodsOptions: apigateway.MethodOptions, sharedResponseModels: apigateway.MethodResponse[], responseModels: DrugMethodResponses, responseModelKey: string, apiModels: DrugApiModels,
        usedAuthorizationType: apigateway.AuthorizationType): apigateway.Resource => {
        
        let newResourcePath = ''
        let functionARN = ''
        let resourceFunctionName = ''

        switch(drugResource) {
            case DrugResource.DRUG:
                newResourcePath = 'drug'
                functionARN = 'arn:aws:lambda:us-east-2:599822976138:function:DrugInfoStack-birdirxdruginfoC9BDCBEE-0Wnrr4E84ob8'
                resourceFunctionName = 'drug-lambda'
            break
            case DrugResource.DRUGLOOKUP:
                newResourcePath = 'druglookup'
                functionARN = 'arn:aws:lambda:us-east-2:599822976138:function:DrugLookupStack-birdirxdruglookup23E44297-JXS9F3TmcU6i'
                resourceFunctionName = 'drug-lookup-lambda'
            break
            case DrugResource.DRUG_DESCRIPTION:
                newResourcePath = 'description'
                functionARN = 'arn:aws:lambda:us-east-2:599822976138:function:DrugDescriptionStack-birdirxdrugdescription9A181BE-e7Kwo4Uxff1z'
                resourceFunctionName = 'drug-description-lambda'
            break
            case DrugResource.DRUG_PRICING:
                newResourcePath = 'pricebyndc'
                functionARN = 'arn:aws:lambda:us-east-2:599822976138:function:DrugPricingStack-birdirxdrugpricing2380FD7F-15JqMsR22HJ2'
                resourceFunctionName = 'drug-price-by-ndc-lambda'
            break
            case DrugResource.DRUG_PACKAGING:
                newResourcePath = 'packaging'
                functionARN = 'arn:aws:lambda:us-east-2:599822976138:function:DrugPackagingStack-birdirxdrugpackaging76E4033A-46GLI1jyMBap'
                resourceFunctionName = 'drug-packaging-lambda'
            break
            case DrugResource.DRUG_IMAGE:
                newResourcePath = 'image'
                functionARN = 'arn:aws:lambda:us-east-2:599822976138:function:DrugImageStack-birdirxdrugimage3B0F054B-PCGDpu1GJtZ6'
                resourceFunctionName = 'drug-image-lambda'
            break
            case DrugResource.DRUG_FLAGS:
                newResourcePath = 'flags'
                functionARN = 'arn:aws:lambda:us-east-2:599822976138:function:DrugFlagsInfo-birdirxdrugfiltersinfo0BDC4D41-Y2cev9LiTQ57'
                resourceFunctionName = 'drug-flags-lambda'
            break

            default:
                throw Error('switch case not defined')

        }
            
        
        if (newResourcePath === '' || functionARN === '' || resourceFunctionName === '') {
            throw Error('Resource not configured properly on switch case')
        }

        functionARN = 'arn:aws:lambda:us-east-2:070307939360:function:TestLambda'
            
        const newResource = parentResource.addResource(newResourcePath)
        const resourceFunction = lambda.Function.fromFunctionArn(this, resourceFunctionName , functionARN)
        const functionIntegration = new apigateway.LambdaIntegration(resourceFunction, lambdaIntegrationOptions)


        this.methodsSetup(drugResource, sharedMethodsOptions, sharedResponseModels, responseModels, responseModelKey, apiModels, newResource, functionIntegration, usedAuthorizationType)

        return newResource


    }

    methodsSetup = (drugResource: DrugResource,
        sharedMethodsOptions: apigateway.MethodOptions, sharedResponseModels: apigateway.MethodResponse[], responseModels: DrugMethodResponses, responseModelKey: string, apiModels: DrugApiModels,
        resourceObject: apigateway.Resource, functionIntegration: apigateway.LambdaIntegration, usedAuthorizationType: apigateway.AuthorizationType) => {            
            
            const resourceMethodOptions = {...sharedMethodsOptions}
            
            switch(drugResource) {
                case DrugResource.DRUG:
                    
                    resourceMethodOptions.methodResponses = [
                        ...sharedResponseModels,
                        {
                            ...responseModels.method200SharedResponse,
                            responseModels: {
                                [responseModelKey]: apiModels.drugResponseModel
                            }
                        }
                    
                    ]
                    resourceObject.addMethod('GET', functionIntegration, {
                        ...resourceMethodOptions,
                        authorizationType: usedAuthorizationType,
                        requestParameters: {
                            'method.request.querystring.name': true,
                            'method.request.querystring.cs-code': false,
                            'method.request.querystring.dc-code': false,
                            'method.request.querystring.item-status': false,
                            'method.request.querystring.rtx-code': false,
                        },
                    })
                break
                case DrugResource.DRUGLOOKUP:

                    resourceMethodOptions.methodResponses = [
                        ...sharedResponseModels,
                        {
                            ...responseModels.method200SharedResponse,
                            responseModels: {
                                [responseModelKey]: apiModels.drugLookupResponseModel
                            }
                        }
                        
                    ]

                    resourceObject.addMethod('GET', functionIntegration, {
                        ...resourceMethodOptions,
                        authorizationType: usedAuthorizationType,
                        requestParameters: {
                            'method.request.querystring.search-name': true,
                            'method.request.querystring.cs-code': false,
                            'method.request.querystring.dc-code': false,
                            'method.request.querystring.item-status': false,
                            'method.request.querystring.rtx-code': false,
                        },
                    })
                break
                case DrugResource.DRUG_DESCRIPTION:

                    resourceMethodOptions.methodResponses = [
                        ...sharedResponseModels,
                        {
                            ...responseModels.method200SharedResponse,
                            responseModels: {
                                [responseModelKey]: apiModels.drugDescriptionResponseModel
                            }
                        }
                        
                    ]

                    resourceObject.addMethod('GET', functionIntegration, {
                        ...resourceMethodOptions,
                        authorizationType: usedAuthorizationType,
                        requestParameters: {
                            'method.request.querystring.ddi': true,
                            'method.request.querystring.gpi': true,
                            'method.request.querystring.lang': false,
                        },
                    })
                break
                case DrugResource.DRUG_PRICING:

                    resourceMethodOptions.methodResponses = [
                        ...sharedResponseModels,
                        {
                            ...responseModels.method200SharedResponse,
                            responseModels: {
                                [responseModelKey]: apiModels.drugPricingResponseModel
                            }
                        }
                    ]

                    resourceObject.addMethod('GET', functionIntegration, {
                        ...resourceMethodOptions,
                        authorizationType: usedAuthorizationType,
                        requestParameters: {
                            'method.request.querystring.ndc': true,
                            'method.request.querystring.price-code': false,
                        },
                    })
                break
                case DrugResource.DRUG_PACKAGING:

                    resourceMethodOptions.methodResponses = [
                        ...sharedResponseModels,
                        {
                            ...responseModels.method200SharedResponse,
                            responseModels: {
                                [responseModelKey]: apiModels.drugPackagingResponseModel
                            }
                        }
                        
                    ]

                    resourceObject.addMethod('GET', functionIntegration, {
                        ...resourceMethodOptions,
                        authorizationType: usedAuthorizationType,
                        requestParameters: {
                            'method.request.querystring.ndc': true
                        },
                    })
                break
                case DrugResource.DRUG_IMAGE:

                    resourceMethodOptions.methodResponses = [
                        ...sharedResponseModels,
                        {
                            ...responseModels.method200SharedResponse,
                            responseModels: {
                                [responseModelKey]: apiModels.drugImageResponseModel
                            }
                        }
                        
                    ]

                    resourceObject.addMethod('GET', functionIntegration, {
                        ...resourceMethodOptions,
                        authorizationType: usedAuthorizationType,
                        requestParameters: {
                            'method.request.querystring.ndc': true
                        },
                    })
                break
                case DrugResource.DRUG_FLAGS:

                    resourceMethodOptions.methodResponses = [
                        ...sharedResponseModels,
                        {
                            ...responseModels.method200SharedResponse,
                            responseModels: {
                                [responseModelKey]: apiModels.drugFlagsResponseModel
                            }
                        }
                        
                    ]

                    resourceObject.addMethod('GET', functionIntegration, {
                        ...resourceMethodOptions,
                        authorizationType: usedAuthorizationType,
                        requestParameters: {
                            'method.request.querystring.name': true,
                        },
                    })
                break

                default:
                    throw Error('switch case not defined')
        }

        resourceObject.addCorsPreflight({
            allowOrigins: ['*'],
        })
       
    }


}
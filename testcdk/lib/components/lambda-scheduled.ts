import { TestcdkStack } from './../testcdk-stack';

import * as cdk from '@aws-cdk/core';
import * as cognito from "@aws-cdk/aws-cognito";
import * as iam from "@aws-cdk/aws-iam";
import * as lambda from "@aws-cdk/aws-lambda";
import * as events from "@aws-cdk/aws-events";
import { CfnOutput, Duration } from '@aws-cdk/core';
import { UserPool } from '@aws-cdk/aws-cognito';
import { PolicyStatement } from '@aws-cdk/aws-iam';
import outputsJson from '../../resources/outputs/outputs.json'

export class LambdaScheduled extends cdk.Construct {

    constructor(scope: cdk.Construct, id: string, props?: any) {
        super(scope, id)
  
  
        const lambdaRuntimes = lambda.Runtime.NODEJS_14_X
  
        const lambdaExecutionRole = new iam.Role(this, 'LambdaRole', 
          {
              // assumedBy: new AccountPrincipal(stack.account)
              assumedBy: new iam.ServicePrincipal('lambda.amazonaws.com'),
              managedPolicies: [
                  iam.ManagedPolicy.fromAwsManagedPolicyName('service-role/AWSLambdaVPCAccessExecutionRole'),
                //   iam.ManagedPolicy.fromManagedPolicyArn(this, 'AmazonSSMFullAccess', 'arn:aws:iam::aws:policy/AmazonSSMFullAccess')
              ],
          })
          
        const lambdaEnvironmentVariables = {
            TIMEOUT_TOLERANCE_MILLIS: "10",
        }

        const eventRule = new events.Rule(this, 'scheduleRule', {
            schedule: events.Schedule.cron({ minute: '1', hour: '0' }),
        })

        // const schedule = events.Schedule.rate(Duration.seconds(30))
  
  
        const lambdaFunction = new lambda.Function(this, "scheduledLambda", {
            code: lambda.Code.fromAsset("resources/lambdas/scheduledFunction"),
            handler: "scheduledFunction.scheduledFunction",
            environment: {
              ...lambdaEnvironmentVariables,
            },
            runtime: lambdaRuntimes,
            role: lambdaExecutionRole
            
        })
      }
}
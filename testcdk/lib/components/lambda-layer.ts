import { TestcdkStack } from './../testcdk-stack';

import * as cdk from '@aws-cdk/core';
import * as cognito from "@aws-cdk/aws-cognito";
import * as iam from "@aws-cdk/aws-iam";
import * as lambda from "@aws-cdk/aws-lambda";
import { CfnOutput, Duration } from '@aws-cdk/core';
import { UserPool } from '@aws-cdk/aws-cognito';
import { PolicyStatement } from '@aws-cdk/aws-iam';
import outputsJson from '../../resources/outputs/outputs.json'

export class LambdaLayer extends cdk.Construct {

    constructor(scope: cdk.Construct, id: string, props?: cdk.StackProps) {
        super(scope, id)

        console.log('lambda layer arn', outputsJson.TestCdkStack.TestcdkStackLambdaLayerA48B7145)

        const layer: lambda.LayerVersion = new lambda.LayerVersion(this, "my_layer", {
            code: lambda.Code.fromAsset("resources/lambdas/layers/testLayer/nodejs.zip"),
            compatibleRuntimes: [lambda.Runtime.NODEJS_14_X]
        })

        new cdk.CfnOutput(this, 'LambdaLayer', {
            value: layer.layerVersionArn,
            description: 'The arn of the Lambda Layer',
            exportName: 'LambdaLayerARN',
        })
    }
}
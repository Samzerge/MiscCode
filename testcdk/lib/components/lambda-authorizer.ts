import * as cdk from '@aws-cdk/core';
import * as lambda from "@aws-cdk/aws-lambda";

export class LambdaAuthorizer extends cdk.Construct {

    public lambdaRuntime: lambda.Runtime
    public implementationFunction: lambda.Function

    constructor(scope: cdk.Construct, id: string, props?: cdk.StackProps) {
        super(scope, id)

        this.lambdaRuntime = lambda.Runtime.NODEJS_14_X

        this.implementationFunction = new lambda.Function(this, "TestFunction", {
            runtime: this.lambdaRuntime,
            code: lambda.Code.fromAsset("resources/lambdas/authorizer"),
            handler: "authorizer.authorizer",

        })


    }
}
import { TestcdkStack } from '../testcdk-stack';

import * as cdk from '@aws-cdk/core';
import * as cognito from "@aws-cdk/aws-cognito";
import * as iam from "@aws-cdk/aws-iam";
import * as lambda from "@aws-cdk/aws-lambda";
import * as glue from "@aws-cdk/aws-glue";
import * as s3 from '@aws-cdk/aws-s3';
import * as s3Deployment from '@aws-cdk/aws-s3-deployment';
import { CfnOutput, Duration } from '@aws-cdk/core';
import { UserPool } from '@aws-cdk/aws-cognito';
import { ManagedPolicy, PolicyStatement } from '@aws-cdk/aws-iam';
import outputsJson from '../../resources/outputs/outputs.json'

export class GlueConstruct extends cdk.Construct {

    constructor(scope: cdk.Construct, id: string, props?: cdk.StackProps) {
        super(scope, id)

        const useOwnBucket = true

        const glueRole = new iam.Role(this, 'AWSGlueServiceRole-TestGlueServiceRole', 
        {
            // assumedBy: new AccountPrincipal(stack.account)
            assumedBy: new iam.ServicePrincipal('glue.amazonaws.com'),
            managedPolicies: [
                iam.ManagedPolicy.fromAwsManagedPolicyName(
                    'service-role/AWSGlueServiceRole',
                ),
                iam.ManagedPolicy.fromManagedPolicyArn(this, 'AmazonS3FullAccessPolicy',
                    'arn:aws:iam::aws:policy/AmazonS3FullAccess',
                ),
            ]
        })
        
        const s3ReadWriteBucket = new s3.Bucket(this, 'GlueDemoBucket', {
            bucketName: 'glue-demo-bucket-samz-92',
            removalPolicy: cdk.RemovalPolicy.DESTROY
        })

        const dependenciesDeployment = new s3Deployment.BucketDeployment(this, "dependencies-deployment", {
            sources: [s3Deployment.Source.asset("./resources/s3/S3GlueCrawlerFolder")],
            destinationBucket: s3ReadWriteBucket,
            retainOnDelete: false
        })

        const preexistingBucket = s3.Bucket.fromBucketArn(this, 'GluePreexistingBucket', 'arn:aws:s3:::awsglue-datasets')


        let usedBucket= null
        let appendBucketPath = ''
        if (useOwnBucket) {

            usedBucket = s3ReadWriteBucket
            appendBucketPath = 'read'
            
        } else {
            // s3://awsglue-datasets/examples/medicare/Medicare_Hospital_Provider.csv
            usedBucket = preexistingBucket
            appendBucketPath = 'examples/medicare'
                                    // path: `s3://awsglue-datasets/examples/medicare/Medicare_Hospital_Provider.csv`,
                        // path: 'arn:aws:s3:::awsglue-datasets/examples/medicare'
        }

        const databaseName = 'glue-demo-database'
        // const tableName = 'glue-demo-table'
        const glueDatabase = new glue.Database(this, 'GlueDemoDatabase', {
            databaseName: databaseName,
        })
        // const glueTable = new glue.Table(this, 'GlueDemoTable', {
        //     tableName: 'glue-demo-table',
        //     description: 'Test table',
        //     database: glueDatabase,
        //     bucket: s3ReadWriteBucket,
        //     s3Prefix: 'glue-demo',
        //     columns: [],
        //     dataFormat: {
        //         inputFormat: glue.InputFormat.TEXT,
        //         outputFormat: glue.OutputFormat.ORC,
        //         serializationLibrary: glue.SerializationLibrary.OPEN_CSV
        //     }


        // })

        const s3TargetPath = `${usedBucket.bucketName}/${appendBucketPath}`
        console.log('s3TargetPath', s3TargetPath)
        const glueCrawler = new glue.CfnCrawler(this, 'GlueDemoCrawler', {
            role: glueRole.roleArn,
            name: 'glue-demo-crawler',
            databaseName: databaseName,
            description: 'test glue crawler',
            targets: {
                s3Targets: [
                    {
                        path: s3TargetPath,
                        // path: `${usedBucket.urlForObject(appendBucketPath)}`,
                        // path: `${usedBucket.bucketName}/${appendBucketPath}`,

                    }
                ]
            }
        })

        const PYTHON_VERSION = "3.0";
        const GLUE_VERSION = "2.0";

        

        const glueJob = new glue.CfnJob(this, 'GlueDemoJob', {
            glueVersion: GLUE_VERSION,
            // pythonVersion: PYTHON_VERSION,
            command: {
                name: 'glue-demo-command',
                pythonVersion: PYTHON_VERSION,
                scriptLocation: `s3://${s3ReadWriteBucket.bucketName}/scripts/script.py`
            },
            role: glueRole.roleArn,
            name: 'glue-demo-job',
            timeout: 5,
        })

        const glueTrigger = new glue.CfnTrigger(this, "GlueDemoTrigger", {
            name: "glue-demo-etl-trigger",
            schedule: "cron(5 * * * ? *)",
            type: "SCHEDULED",
            actions: [
                {
                    jobName: glueJob.name
                }
            ],
            startOnCreation: true
        });
        glueTrigger.addDependsOn(glueJob);


        new cdk.CfnOutput(this, 'ExternalBucketName', {
            value: preexistingBucket.bucketName,
            description: 'The name of the preexisting bucket',
            exportName: 'ExternalBucketName',
        })


        new cdk.CfnOutput(this, 's3TargetPath', {
            value: s3TargetPath,
            description: 'The glue s3 target',  
            exportName: 's3TargetPath',
        })
    }
}
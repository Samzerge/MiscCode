import { TestcdkStack } from './../testcdk-stack';
import * as core from "@aws-cdk/core";
import * as cdk from '@aws-cdk/core';
import * as cognito from "@aws-cdk/aws-cognito";
import * as iam from "@aws-cdk/aws-iam";
import * as lambda from "@aws-cdk/aws-lambda";
import * as events from "@aws-cdk/aws-events";
import { CfnOutput, Duration, RemovalPolicy } from '@aws-cdk/core';
import { UserPool } from '@aws-cdk/aws-cognito';
import { PolicyStatement } from '@aws-cdk/aws-iam';
import outputsJson from '../../resources/outputs/outputs.json'
import { Alias, Version } from '@aws-cdk/aws-lambda';

export class SimpleLambdaStack extends cdk.Stack {

    functionName = 'scheduledLambda'

    constructor(scope: cdk.Construct, id: string, props?: any) {
        super(scope, id)

        const versionIdentifier = 'stage'
  
  
        const lambdaRuntimes = lambda.Runtime.NODEJS_14_X
  
        const lambdaExecutionRole = new iam.Role(this, 'LambdaRole', 
          {
              // assumedBy: new AccountPrincipal(stack.account)
              assumedBy: new iam.ServicePrincipal('lambda.amazonaws.com'),
              managedPolicies: [
                  iam.ManagedPolicy.fromAwsManagedPolicyName('service-role/AWSLambdaVPCAccessExecutionRole'),
                //   iam.ManagedPolicy.fromManagedPolicyArn(this, 'AmazonSSMFullAccess', 'arn:aws:iam::aws:policy/AmazonSSMFullAccess')
              ],
          })
          
        const lambdaEnvironmentVariables = {
            TIMEOUT_TOLERANCE_MILLIS: "10",
            DEPLOYED_STAGE: "stage"
        }
  
  
        const lambdaFunction = new lambda.Function(this, "scheduledLambda", {
            code: lambda.Code.fromAsset("resources/lambdas/scheduledFunction"),
            handler: "scheduledFunction.scheduledFunction",
            environment: {
              ...lambdaEnvironmentVariables,
            },
            runtime: lambdaRuntimes,
            role: lambdaExecutionRole
            
        })



        const servicePrincipal = new iam.ServicePrincipal('apigateway.amazonaws.com')
        const sourceRegion = 'us-east-2'
        const sourceAccount = '070307939360'
        const restApiId = 'be9sp0mzdh'
        const path = 'test'

        const sourceApiId = `${restApiId}/*/GET/${path}`
        const sourceArn = `arn:aws:execute-api:${sourceRegion}:${sourceAccount}:${sourceApiId}`

        lambdaFunction.addPermission(`api-permission-${this.functionName}`, {
            action: 'lambda:InvokeFunction',
            principal: servicePrincipal,
            sourceArn: sourceArn,
            sourceAccount: sourceAccount
        })
        

        // const testLambdaVQA = new Version(this, `${this.functionName}-QA`, {
        //     lambda: lambdaFunction
        // });
        
        // const testLambdaVQAAlias = new Alias(this, `${this.functionName}-QA-Alias`, {
        //     aliasName: `${this.functionName}-QA`,
        //     version: testLambdaVQA
        // })
        const devLambdaVDev = new Version(this, `${this.functionName}-dev`, {
            description: 'The dev environment version',
            lambda: lambdaFunction,
            // removalPolicy: RemovalPolicy.RETAIN
        });
        const devLambdaV2Dev = new Version(this, `${this.functionName}-2-dev`, {
            description: 'The dev environment version updated api permissions',
            lambda: lambdaFunction,
            // removalPolicy: RemovalPolicy.RETAIN
        });
        const lambdavQA = new Version(this, `${this.functionName}-qa`, {
            description: 'The qa environment version',
            lambda: lambdaFunction,
            // removalPolicy: RemovalPolicy.RETAIN
        });
        const lambdavStage = new Version(this, `${this.functionName}-stage`, {
            description: 'The stage environment version',
            lambda: lambdaFunction,
            // removalPolicy: RemovalPolicy.RETAIN
        });


        const currentDevVersion = devLambdaV2Dev
        const currentQaVErsion = lambdavQA
        const currentStageVersion = lambdavStage

        const devLambdaVDevAlias = new Alias(this, `${this.functionName}-dev-alias`, {
            description: 'The dev environment version alias',
            aliasName: `${this.functionName}-dev`,
            version: currentDevVersion,
        })
        const devLambdaVQAAlias = new Alias(this, `${this.functionName}-qa-alias`, {
            description: 'The qa environment version alias',
            aliasName: `${this.functionName}-qa`,
            version: currentQaVErsion,
        })
        const devLambdaVStageAlias = new Alias(this, `${this.functionName}-stage-alias`, {
            description: 'The stage environment version alias',
            aliasName: `${this.functionName}-stage`,
            version: currentStageVersion,
        })



        const arnOutputName = `${this.functionName}-ARN`
        const devAssociatedVersionName = `${this.functionName}-dev-version`
        const devVersionAliasName= `${devLambdaVDevAlias.aliasName}-ARN`
        const qaAssociatedVersionName = `${this.functionName}-qa-version`
        const qaVersionAliasName= `${devLambdaVQAAlias.aliasName}-ARN`
        const stageAssociatedVersionName = `${this.functionName}-stage-version`
        const stageVersionAliasName= `${devLambdaVStageAlias.aliasName}-ARN`

        // console.log('devVersionName', devVersionName)
        // console.log('devLambdaVDev.functionName', devLambdaVDev.functionName)
        // console.log('devVersionAliasName', devVersionAliasName)

        // devLambdaVDev.latestVersion

        new core.CfnOutput(this, arnOutputName, {
            value: lambdaFunction.functionArn,
            description: `The arn of the Lambda: ${this.functionName}`,
            exportName: arnOutputName,
        })
        new core.CfnOutput(this, devVersionAliasName, {
            value: devLambdaVDevAlias.functionArn,
            description: `The arn of the Lambda alias: ${devVersionAliasName}`,
            exportName: devVersionAliasName,
        })
        new core.CfnOutput(this, devAssociatedVersionName, {
            value: currentDevVersion.version,
            description: `The version of the Lambda alias: ${devVersionAliasName}`,
            exportName: devAssociatedVersionName,
        })
        new core.CfnOutput(this, qaVersionAliasName, {
            value: devLambdaVQAAlias.functionArn,
            description: `The arn of the Lambda alias: ${qaVersionAliasName}`,
            exportName: qaVersionAliasName,
        })
        new core.CfnOutput(this, qaAssociatedVersionName, {
            value: currentQaVErsion.version,
            description: `The version of the Lambda alias: ${qaVersionAliasName}`,
            exportName: qaAssociatedVersionName,
        })
        new core.CfnOutput(this, stageVersionAliasName, {
            value: devLambdaVStageAlias.functionArn,
            description: `The arn of the Lambda alias: ${stageVersionAliasName}`,
            exportName: stageVersionAliasName,
        })
        new core.CfnOutput(this, stageAssociatedVersionName, {
            value: currentStageVersion.version,
            description: `The version of the Lambda alias: ${stageVersionAliasName}`,
            exportName: stageAssociatedVersionName,
        })
        
    }
}
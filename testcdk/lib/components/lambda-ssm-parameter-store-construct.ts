
import * as cdk from '@aws-cdk/core';
import * as lambda from "@aws-cdk/aws-lambda";
import * as ec2 from '@aws-cdk/aws-ec2';
import * as iam from "@aws-cdk/aws-iam";

export class LambdaSsmParameterStoreConstruct extends cdk.Construct {
    constructor(scope: cdk.Construct, id: string, props?: any) {
      super(scope, id)


      const lambdaRuntimes = lambda.Runtime.NODEJS_14_X

      const lambdaExecutionRole = new iam.Role(this, 'LambdaRole', 
        {
            // assumedBy: new AccountPrincipal(stack.account)
            assumedBy: new iam.ServicePrincipal('lambda.amazonaws.com'),
            managedPolicies: [
                iam.ManagedPolicy.fromAwsManagedPolicyName('service-role/AWSLambdaVPCAccessExecutionRole'),
                iam.ManagedPolicy.fromManagedPolicyArn(this, 'AmazonSSMFullAccess', 'arn:aws:iam::aws:policy/AmazonSSMFullAccess')
            ],
        })
        
      const lambdaEnvironmentVariables = {
          TIMEOUT_TOLERANCE_MILLIS: "10",
          SSM_PARAMETER_RETRIEVE_NAME: 'test-parameter',
          SSM_PARAMETER_VALUE: 'ABC1234',
          
      }


      const lambdaFunction = new lambda.Function(this, "ssmParameterAccess", {
          code: lambda.Code.fromAsset("resources/lambdas/ssmParameterAccess"),
          handler: "ssmParameterAccess.ssmParameterAccess",
          environment: {
            ...lambdaEnvironmentVariables,
          },
          runtime: lambdaRuntimes,
          role: lambdaExecutionRole
          
      })
    }
}
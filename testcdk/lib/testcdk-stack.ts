import { LambdaS3ImageRetrievalConstruct } from './lambda-s3-image-retrieval';

import { ApiSecurityConstruct } from './api-security-construct';
import * as cdk from '@aws-cdk/core';
import { CognitoConstruct } from './cognito-construct';
import { ApiIamRoleConstruct } from './api-iam-role-construct';
import { LambdaLayer } from './components/lambda-layer';
import { ExistingApiConstruct } from './existing-api-construct';
import { LambdaS3AccessCognitoCredentialsConstruct } from './lambda-s3-access-cognito-credentials';

export class TestcdkStack extends cdk.Stack {
  constructor(scope: cdk.Construct, id: string, props?: cdk.StackProps) {
    super(scope, id, props);

    // new testcdk_construct.TestcdkConstruct(this, 'TestCdk');

    // const cognitoConstruct = new CognitoConstruct(this, 'Test-Cognito')
    // const apiSecurityConstruct = new ApiSecurityConstruct(this, 'Test-Api', {
    //   userPool: cognitoConstruct.userPool
    // })

    // const apiIamConstruct = new ApiIamRoleConstruct(this, 'Test-Api-Iam')

    // new LambdaS3ImageRetrievalConstruct(this, 'LambdaS3ImageRetrieval')
    // new ApiSecurityConstruct(this, 'api')

    // new LambdaLayer(this, 'TestcdkStack')
    // TestcdkStackmylayerF9464F5
    
    new ExistingApiConstruct(this, 'exisitngAPiTest')
    // new LambdaS3AccessCognitoCredentialsConstruct(this, 'LambdaS3AccessCognitoCredentialsConstruct')
  }
}

import * as core from "@aws-cdk/core";
import * as apigateway from "@aws-cdk/aws-apigateway";
import * as lambda from "@aws-cdk/aws-lambda";
import * as s3 from '@aws-cdk/aws-s3';
import * as s3Deployment from '@aws-cdk/aws-s3-deployment';
import * as ec2 from '@aws-cdk/aws-ec2';
import * as iam from "@aws-cdk/aws-iam";
import * as cognito from "@aws-cdk/aws-cognito"
import { PolicyStatement } from "@aws-cdk/aws-iam";
import { privateEncrypt } from "crypto";

export class LambdaS3AccessCognitoCredentialsConstruct extends core.Construct {


    constructor(scope: core.Construct, id: string) {
        super(scope, id)

        const lambdaRuntimes = lambda.Runtime.NODEJS_14_X

        
        const identityPool = new cognito.CfnIdentityPool(this, 'IdentityPool', {
            allowUnauthenticatedIdentities: true,
            cognitoIdentityProviders: [
            ],
        })



        // https://testcdkstack-lambdas3imageretrievaltestbucket4d6a-8cqgqypq9bph.s3.us-east-2.amazonaws.com/spacejam2.jpg
        const vpc = new ec2.Vpc(this, 'TestVPC',
            {
                cidr: "10.0.0.0/16"
            }
        )

        const securityGroup = new ec2.SecurityGroup(this, 'TestSecurityGroup',
            {
                vpc: vpc
            }
        )

        
        const privateBucket = new s3.Bucket(this, 'privateBucket', {
            encryption: s3.BucketEncryption.UNENCRYPTED,
            // blockPublicAccess: {
            //     blockPublicAcls: true,
            //     blockPublicPolicy: true,
            //     ignorePublicAcls: true,
            //     restrictPublicBuckets: true
            // },
            
          
        })

        
        const lambdaEnvironmentVariables = {
            TIMEOUT_TOLERANCE_MILLIS: "10",
            PRIVATE_BUCKET: privateBucket.bucketName,
            PRIVATE_KEY: "subfolder/spiderman.jpg",
            COGNITO_REGION: "us-east-2",
            COGNITO_IDENTITY_POOL_ID: "us-east-2:eec750e6-1b6d-4d09-a36e-22b6b8b8ca3c"
        }

        const lambdaS3PolicyDocumentTitle = 'lambdaS3PolicyDocument'

        const s3GetObjectPolicyStatement = new iam.PolicyStatement({
            effect: iam.Effect.ALLOW,
            actions: ['s3:GetObject'],
            resources: [
                `${privateBucket.bucketArn}/*`,
                // `${privateBucket.bucketArn}/subfolder/*`,
                // 'arn:aws:s3:::testcdkstack-lambdas3accesscognitocredentialscons-1lzgnn8u9qs2m/'
            ]
            // arn:aws:s3:::testcdkstack-lambdas3accesscognitocredentialscons-1lzgnn8u9qs2m
        })

        const seGetObjectPolicyDocument = new iam.PolicyDocument({
            statements: [
                s3GetObjectPolicyStatement
            ]
        })
        

        const authRole = new iam.Role(this, 'CognitoAuthRole', {
            description: 'Default role for authenticated users',
            assumedBy: new iam.FederatedPrincipal(
                'cognito-identity.amazonaws.com',
                {
                    StringEquals: {
                        'cognito-identity.amazonaws.com:aud': identityPool.ref,
                    },
                    'ForAnyValue:StringLike': {
                        'cognito-identity.amazonaws.com:amr': 'authenticated',
                    },
                },
                'sts:AssumeRoleWithWebIdentity',
            ),
            managedPolicies: [
                iam.ManagedPolicy.fromAwsManagedPolicyName(
                    'service-role/AWSLambdaBasicExecutionRole',
                ),
            ],
            inlinePolicies: {
                [lambdaS3PolicyDocumentTitle]: seGetObjectPolicyDocument
            }
        })
        const unAuthRole = new iam.Role(this, 'CognitoUnAuthRole', {
            description: 'Default role for authenticated users',
            assumedBy: new iam.FederatedPrincipal(
                'cognito-identity.amazonaws.com',
                {
                    StringEquals: {
                        'cognito-identity.amazonaws.com:aud': identityPool.ref,
                    },
                    'ForAnyValue:StringLike': {
                        'cognito-identity.amazonaws.com:amr': 'unauthenticated',
                    },
                },
                'sts:AssumeRoleWithWebIdentity',
            ),
            managedPolicies: [
                iam.ManagedPolicy.fromAwsManagedPolicyName(
                    'service-role/AWSLambdaBasicExecutionRole',
                ),
            ],
            inlinePolicies: {
                [lambdaS3PolicyDocumentTitle]: seGetObjectPolicyDocument
            }
        })

        const authPolicy = new PolicyStatement({
            effect: iam.Effect.ALLOW,
            actions: [
                "mobileanalytics:PutEvents",
                "cognito-sync:*",
                "cognito-identity:*",
                "execute-api:Invoke"
            ],
            resources: [
                "*"
            ]
        })
        authRole.addToPolicy(authPolicy)

        const unAuthPolicy = new PolicyStatement({
            effect: iam.Effect.ALLOW,
            actions: [
                "mobileanalytics:PutEvents",
                "cognito-sync:*",
                "cognito-identity:*",
                "execute-api:Invoke"
            ],
            resources: [
                "*"
            ]
        })
        unAuthRole.addToPolicy(unAuthPolicy)

        const identityPoolRoleAttachment = new cognito.CfnIdentityPoolRoleAttachment(this, 'IdentityPoolRoleAttachment', {
            identityPoolId: identityPool.ref,
            roles: {
                authenticated: authRole.roleArn,
                unauthenticated: unAuthRole.roleArn,
            }
        })
    
        
        



        const privateBucketDeployment = new s3Deployment.BucketDeployment(this, 'privateBucketDeployment', {
            sources: [s3Deployment.Source.asset('./resources/s3/lambdaS3ImageRetrievalFolder/')], 
            destinationBucket: privateBucket,
        })

        console.log('privateBucket.bucketArn', privateBucket.bucketArn)
        
        

        const lambdaExecutionRole = new iam.Role(this, 'LambdaRole', 
        {
            // assumedBy: new AccountPrincipal(stack.account)
            assumedBy: new iam.ServicePrincipal('lambda.amazonaws.com'),
            managedPolicies: [
                iam.ManagedPolicy.fromAwsManagedPolicyName('service-role/AWSLambdaVPCAccessExecutionRole'),
            ],
            inlinePolicies: {
                [lambdaS3PolicyDocumentTitle]: seGetObjectPolicyDocument
            }
        })

        const imageS3CognitoFunction = new lambda.Function(this, "getImageS3Cognito", {
            code: lambda.Code.fromAsset("resources/lambdas/getImageS3Cognito"),
            handler: "getImageS3Cognito.getImageS3Cognito",
            environment: {
              ...lambdaEnvironmentVariables,
              "NAME": "getImageS3Cognito"
            },
            runtime: lambdaRuntimes,
            securityGroups: [securityGroup],
            vpc: vpc,
            role: lambdaExecutionRole
            
        })
        const imageLambdaS3Function = new lambda.Function(this, "getImageLambdaS3", {
            code: lambda.Code.fromAsset("resources/lambdas/getImageLambdaS3"),
            handler: "getImageLambdaS3.getImageLambdaS3",
            environment: {
              ...lambdaEnvironmentVariables,
              "NAME": "getImageLambdaS3"
            },
            runtime: lambdaRuntimes,
            securityGroups: [securityGroup],
            vpc: vpc,
            role: lambdaExecutionRole
        })


        // const api = new apigateway.RestApi(this, "TestApi", {
        //     restApiName: "Test Api CDK",
        //     description: "This service is the api.",
        // });
        
        
        // const imageAndInfoIntegration = new apigateway.LambdaIntegration(imageAndInfoFunction, {
        //     // requestParameters: {
        //         //   "search-name": ""
        //         // },
        //         allowTestInvoke: true,
        //         requestTemplates: { "application/json": '{ "statusCode": "200" }' },
                
        // });
        // api.root.addMethod('GET', imageAndInfoIntegration)
            

        new core.CfnOutput(this, 'BucketName', {
            value: privateBucket.bucketName,
            description: 'The name of th ebucket',
            exportName: 'BucketName',
        })
        
    }
}
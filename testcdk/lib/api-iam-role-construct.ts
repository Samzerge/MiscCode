import * as cdk from '@aws-cdk/core';
import * as apigateway from "@aws-cdk/aws-apigateway";
import * as cognito from "@aws-cdk/aws-cognito";
import * as iam from "@aws-cdk/aws-iam";
import * as lambda from "@aws-cdk/aws-lambda";
import { Duration } from '@aws-cdk/core';
import { PolicyStatement } from '@aws-cdk/aws-iam';


export class ApiIamRoleConstruct extends cdk.Construct {

  constructor(scope: cdk.Construct, id: string, props?: any) {
    super(scope, id);

    const identityPool = new cognito.CfnIdentityPool(this, 'IdentityPool', {
        allowUnauthenticatedIdentities: true,
        cognitoIdentityProviders: [],
    })

    const implementationFunction = new lambda.Function(this, "TestFunction", {
        runtime: lambda.Runtime.NODEJS_14_X,
        code: lambda.Code.fromAsset("resources/lambdas/implFunction"),
        handler: "implFunction.implFunction",

    })

    const restApi = new apigateway.RestApi(this, "TestApi", {
      description: "Test API"
    });

    // const roleProperties = {
    //     description: 'Default role for anonymous users',
    //     assumedBy: new iam.FederatedPrincipal(
    //         'cognito-identity.amazonaws.com',
    //         {
    //             StringEquals: {
    //                 'cognito-identity.amazonaws.com:aud': identityPool.ref,
    //             },
    //             'ForAnyValue:StringLike': {
    //                 'cognito-identity.amazonaws.com:amr': 'authenticated',
    //             },
    //         },
    //         'sts:AssumeRoleWithWebIdentity',
    //     ),
    // }
    // const roleProperties2 = {
    //     description: 'Default role for anonymous users',
    //     assumedBy: new iam.FederatedPrincipal(
    //         'cognito-identity.amazonaws.com',
    //         {
    //             StringEquals: {
    //                 'cognito-identity.amazonaws.com:aud': identityPool.ref,
    //             },
    //             'ForAnyValue:StringLike': {
    //                 'cognito-identity.amazonaws.com:amr': 'authenticated',
    //             },
    //         },
    //         'sts:AssumeRoleWithWebIdentity',
    //     ),
    // }


    // const authRole = new iam.Role(this, 'TestIdentityPoolAuthRole', roleProperties)
    // const unAuthRole = new iam.Role(this, 'TestIdentityPoolUnauthRole', roleProperties)

    // const executeApiPolicy = new PolicyStatement(
    //     {
    //         effect: iam.Effect.ALLOW,
    //         actions: [
    //             "execute-api:Invoke"
    //         ],
    //         resources: [
    //             restApi.arnForExecuteApi()
    //         ]
    //     }
    // )

    

    // authRole.addToPolicy(executeApiPolicy)
    // unAuthRole.addToPolicy(executeApiPolicy)

    // const identityPoolRoleAttachment = new cognito.CfnIdentityPoolRoleAttachment(this, 'IdentityPoolRoleAttachment', {
    //     identityPoolId: identityPool.ref,
    //     roles: {
    //         authenticated: authRole.roleArn,
    //         unauthenticated: unAuthRole.roleArn,
    //     }
    // })

    

    const lambdaIntegration = new apigateway.LambdaIntegration(implementationFunction, {
      requestTemplates: { "application/json": '{ "statusCode": "200" }' },
    });


    const testResourceNone = restApi.root.addResource('test')
    const testMethod = testResourceNone.addMethod('GET', lambdaIntegration, {
      apiKeyRequired: false
    })
    const testResource1= restApi.root.addResource('test-identity-auth')
    const testMethod1 = testResource1.addMethod('GET', lambdaIntegration, {
      // authorizer: apiTokenAuthorizer,
      authorizationType: apigateway.AuthorizationType.IAM,
      apiKeyRequired: false
    })

    

  }
}

import { LambdaS3ImageRetrievalConstruct } from './lambda-s3-image-retrieval';

import { ApiSecurityConstruct } from './api-security-construct';
import * as cdk from '@aws-cdk/core';
import { CognitoConstruct } from './cognito-construct';
import { ApiIamRoleConstruct } from './api-iam-role-construct';
import { LambdaLayer } from './components/lambda-layer';
import { ExistingApiConstruct } from './existing-api-construct';
import { LambdaS3AccessCognitoCredentialsConstruct } from './lambda-s3-access-cognito-credentials';
import { DrugApiConstruct } from './components/drug-api-construct';
import { GlueConstruct } from './components/glue-construct';
import { LambdaSsmParameterStoreConstruct } from './components/lambda-ssm-parameter-store-construct';

export class Test2cdkStack extends cdk.Stack {
  constructor(scope: cdk.Construct, id: string, props?: cdk.StackProps) {
    super(scope, id, props);


    // new DrugApiConstruct(this, 'DrugApiConstruct')
    
    // new LambdaLayer(this, 'LambdaLayer')
    // new ExistingApiConstruct(this, 'exisitngAPiTest')

    // new GlueConstruct(this, 'Glue')

    new LambdaS3ImageRetrievalConstruct(this, 'S3ImageRetrieval')
    // new LambdaSsmParameterStoreConstruct(this, 'SSM-Lambda')
  }
}

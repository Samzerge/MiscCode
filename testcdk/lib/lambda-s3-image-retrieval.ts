import * as core from "@aws-cdk/core";
import * as apigateway from "@aws-cdk/aws-apigateway";
import * as lambda from "@aws-cdk/aws-lambda";
import * as s3 from '@aws-cdk/aws-s3';
import * as s3Deployment from '@aws-cdk/aws-s3-deployment';
import * as ec2 from '@aws-cdk/aws-ec2';
import * as iam from "@aws-cdk/aws-iam";

export class LambdaS3ImageRetrievalConstruct extends core.Construct {


    constructor(scope: core.Construct, id: string) {
        super(scope, id)

        const lambdaRuntimes = lambda.Runtime.NODEJS_14_X
        
        const lambdaEnvironmentVariables = {
            TIMEOUT_TOLERANCE_MILLIS: "10",
            PRIVATE_BUCKET: "testcdkstack-lambdas3imageretrievalprivatebucket4-402ers84dldx",
            PRIVATE_KEY: "spacejam2.jpg",
            PUBLIC_BUCKET: "testcdkstack-lambdas3imageretrievalpublicbuckete6-14qe6i6w98roz",
            PUBLIC_KEY: "subfolder/spiderman.jpg",
            
        }

        // https://testcdkstack-lambdas3imageretrievaltestbucket4d6a-8cqgqypq9bph.s3.us-east-2.amazonaws.com/spacejam2.jpg
        const vpc = new ec2.Vpc(this, 'TestVPC',
            {
                cidr: "10.0.0.0/16"
            }
        )

        const securityGroup = new ec2.SecurityGroup(this, 'TestSecurityGroup',
            {
                vpc: vpc
            }
        )
        

        const lambdaExecutionRole = new iam.Role(this, 'LambdaRole', 
        {
            // assumedBy: new AccountPrincipal(stack.account)
            assumedBy: new iam.ServicePrincipal('lambda.amazonaws.com'),
            managedPolicies: [
                iam.ManagedPolicy.fromAwsManagedPolicyName('service-role/AWSLambdaVPCAccessExecutionRole'),
            ],
        })

        const imageAndInfoFunction = new lambda.Function(this, "ImageAndInfo", {
            code: lambda.Code.fromAsset("resources/lambdas/getImageAndInfo"),
            handler: "getImageAndInfo.getImageAndInfo",
            environment: {
              ...lambdaEnvironmentVariables,
            },
            runtime: lambdaRuntimes,
            securityGroups: [securityGroup],
            vpc: vpc,
            role: lambdaExecutionRole
            
        })


        const api = new apigateway.RestApi(this, "TestApi", {
            restApiName: "Test Api CDK",
            description: "This service is the api.",
        });
        
        
        const imageAndInfoIntegration = new apigateway.LambdaIntegration(imageAndInfoFunction, {
            // requestParameters: {
                //   "search-name": ""
                // },
                allowTestInvoke: true,
                requestTemplates: { "application/json": '{ "statusCode": "200" }' },
                
        });
        api.root.addMethod('GET', imageAndInfoIntegration)
            

        const privateBucket = new s3.Bucket(this, 'privateBucket', {
            encryption: s3.BucketEncryption.UNENCRYPTED,
            publicReadAccess: false,
            removalPolicy: core.RemovalPolicy.DESTROY
        });
        const publicBucket = new s3.Bucket(this, 'publicBucket', {
            encryption: s3.BucketEncryption.UNENCRYPTED,
            publicReadAccess: true,
            removalPolicy: core.RemovalPolicy.DESTROY
        });

        const privateBucketDeployment = new s3Deployment.BucketDeployment(this, 'privateBucketDeployment', {
            sources: [s3Deployment.Source.asset('./resources/s3/lambdaS3ImageRetrievalFolder/')], 
            destinationBucket: privateBucket,
            retainOnDelete: false
        });
        const publicBucketDeployment = new s3Deployment.BucketDeployment(this, 'publicBucketDeployment', {
            sources: [s3Deployment.Source.asset('./resources/s3/lambdaS3ImageRetrievalFolder/')], 
            destinationBucket: publicBucket,
            retainOnDelete: false
        });
        
    }
}
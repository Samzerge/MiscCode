import * as cdk from '@aws-cdk/core';
import * as apigateway from "@aws-cdk/aws-apigateway";
import * as cognito from "@aws-cdk/aws-cognito";
import * as iam from "@aws-cdk/aws-iam";
import * as lambda from "@aws-cdk/aws-lambda";
import * as ec2 from '@aws-cdk/aws-ec2';
import { Duration } from '@aws-cdk/core';
import { JsonSchemaType, JsonSchemaVersion } from '@aws-cdk/aws-apigateway';


export class ExistingApiConstruct extends cdk.Construct {
  public value = 5
  constructor(scope: cdk.Construct, id: string, props?: any) {
    super(scope, id);

    const lambdaRuntime = lambda.Runtime.NODEJS_14_X

    // const implementationFunction = new lambda.Function(this, "TestFunction", {
    //     runtime: lambdaRuntime,
    //     code: lambda.Code.fromAsset("resources/lambdas/implFunction"),
    //     handler: "implFunction.implFunction",

    // })

    // const restApi = apigateway.RestApi.fromRestApiId(this, 'TestApi', 'digpb62fh5')
    
    const restApi: apigateway.IRestApi = apigateway.RestApi.fromRestApiAttributes(this, 'TestApi', {
        restApiId: '7oun6o1r1m',
        rootResourceId: 'h57c6pm2o6',
        
    })

    console.log('REST API = ', restApi)

    const restApiCasted = restApi as apigateway.RestApi


    console.log('cors Headers', apigateway.Cors.DEFAULT_HEADERS.join(','))
    console.log('cors origins', apigateway.Cors.ALL_ORIGINS.join(','))
    console.log('cors ALL_METHODS', apigateway.Cors.ALL_METHODS.join(','))
    
    const gatewayResponse400Properties =  {
        type: apigateway.ResponseType.DEFAULT_4XX,
        responseHeaders: {
           'Access-Control-Allow-Origin': `'${apigateway.Cors.ALL_ORIGINS.join(',')}'`,
           'Access-Control-Allow-Headers': `'${apigateway.Cors.DEFAULT_HEADERS.join(',')}'`,
           'Access-Control-Allow-Methods':  `'${apigateway.Cors.ALL_METHODS.join(',')}'`
        }
    }
    const gatewayResponse500Properties =  {
        type: apigateway.ResponseType.DEFAULT_5XX,
        responseHeaders: {
           'Access-Control-Allow-Origin': `'${apigateway.Cors.ALL_ORIGINS.join(',')}'`,
           'Access-Control-Allow-Headers': `'${apigateway.Cors.DEFAULT_HEADERS.join(',')}'`,
           'Access-Control-Allow-Methods':  `'${apigateway.Cors.ALL_METHODS.join(',')}'`
        }
    }
    
    const requestValidator = new apigateway.RequestValidator(this, 'TestRequestValidator', {
        restApi: restApi,
        validateRequestBody: false,
        validateRequestParameters: true,
    })

        
    const model = new apigateway.Model(this, 'TestModel', {
        modelName:'TestModelName',
        description: 'this is the descirption',
        contentType: 'application/json',
        restApi: restApi,
        schema: {
            title: 'Test Schema',
            schema: JsonSchemaVersion.DRAFT4,
            type: JsonSchemaType.OBJECT,
            properties: {
                'message': {
                    type: JsonSchemaType.STRING
                    }
                }
        }
    })
    
    const methodsProperties: apigateway.MethodOptions = {
        // authorizationType: apigateway.AuthorizationType.IAM,
        apiKeyRequired: false,
        requestValidator,
        methodResponses: [
            {
                statusCode: '200',
                responseModels: {
                    'application/json' : model
                }
            },
            {
                statusCode: '408',
                responseModels: {
                    'application/json' : model
                }
            },
        ],

    }

    const vpc = new ec2.Vpc(this, 'TestVPC',
        {
            cidr: "10.0.0.0/16"
        }
    )

    const securityGroup = new ec2.SecurityGroup(this, 'TestSecurityGroup',
        {
            vpc: vpc
        }
    )
    

    const lambdaExecutionRole = new iam.Role(this, 'LambdaRole', 
    {
        // assumedBy: new AccountPrincipal(stack.account)
        assumedBy: new iam.ServicePrincipal('lambda.amazonaws.com'),
        managedPolicies: [
            iam.ManagedPolicy.fromAwsManagedPolicyName('service-role/AWSLambdaVPCAccessExecutionRole'),
        ],
    })

    const imageAndInfoFunction = new lambda.Function(this, "ImageAndInfo", {
        code: lambda.Code.fromAsset("resources/lambdas/getImageAndInfo"),
        handler: "getImageAndInfo.getImageAndInfo",
        environment: {

        },
        runtime: lambda.Runtime.NODEJS_14_X,
        securityGroups: [securityGroup],
        vpc: vpc,
        role: lambdaExecutionRole
        
    })
    
    const integration = new apigateway.LambdaIntegration(imageAndInfoFunction, {
        allowTestInvoke: true,
        requestTemplates: { "application/json": '{ "statusCode": "200" }' },
            
    });

    const getMethod = restApi.root.addMethod('GET', integration, {
        ...methodsProperties,
        authorizationType: apigateway.AuthorizationType.IAM,
        requestParameters: {
            'method.request.querystring.mandatory': true,
            'method.request.querystring.nonmandatory': false,
        }
        
    })
    const postMethod = restApi.root.addMethod('POST', integration, {
        ...methodsProperties,
        authorizationType: apigateway.AuthorizationType.IAM,
    })
    const existingResource = restApi.root.getResource('existing')
    existingResource?.addMethod('GET', integration, {
        ...methodsProperties,
        authorizationType: apigateway.AuthorizationType.NONE,
        requestParameters: {
            'method.request.querystring.id': true,
            'method.request.querystring.optional': true,
        },
    })
    const existingTestResource = existingResource?.getResource('test')
    existingTestResource?.addMethod('GET', integration, {
        apiKeyRequired: true,
        requestValidator: requestValidator,
        authorizationType: apigateway.AuthorizationType.IAM,
        requestParameters: {
            'method.request.querystring.temp': false,
            'method.request.querystring.temp2': false,
        },
    })
    existingTestResource?.addMethod('POST', integration, {
        apiKeyRequired: true,
        requestValidator: requestValidator,
        authorizationType: apigateway.AuthorizationType.IAM,
        // requestParameters: {
        //     'method.request.querystring.great': true,
        //     'method.request.querystring.magnificient': false,
        // },
    })

    // const temporalResource = restApi.root.addResource('temporal')
    // // temporalResource?.addMethod('OPTIONS', integration, {
    // // })
    // const temporalChildResource = temporalResource.addResource('temporal')
    // temporalChildResource?.addMethod('GET', integration, {
    //     apiKeyRequired: true,
    //     requestValidator: requestValidator,
    //     authorizationType: apigateway.AuthorizationType.IAM,
    //     requestParameters: {
    //         'method.request.querystring.childA': false,
    //         'method.request.querystring.childB': false,
    //     },
    // })


    // const productResource = restApi.root.addResource('product')

    const userResource = restApi.root.addResource('user')
    userResource.addMethod('GET', integration, {
        ...methodsProperties,
        authorizationType: apigateway.AuthorizationType.NONE,
        requestParameters: {
            'method.request.querystring.id': true,
            'method.request.querystring.optional': true,
        },
    })
    userResource.addMethod('POST', integration, {
        ...methodsProperties,
        authorizationType: apigateway.AuthorizationType.NONE,
    })

    // const bookResource = restApi.root.addResource('book')
    
    
    const userProfileResource = userResource.addResource('profile')
    userProfileResource.addMethod('GET',integration, {
        ...methodsProperties,
        authorizationType: apigateway.AuthorizationType.NONE,
        requestParameters: {
            'method.request.querystring.id': true,
            'method.request.querystring.optional': true,
        },

    })
    
    // restApi.root.addCorsPreflight({
    //     allowOrigins: ['*'],
    // })
    // productResource.addCorsPreflight({
    //     allowOrigins: ['*']
    // })
    // bookResource.addCorsPreflight({
    //     allowOrigins: ['*']
    // })
    
    restApiCasted.addGatewayResponse('TestGatewayResponse4XX', gatewayResponse400Properties)
    restApiCasted.addGatewayResponse('TestGatewayResponse5XX', gatewayResponse500Properties)


    const latestDevDeployment = new apigateway.Deployment(this, 'apiDeployment 2', {
        api: restApi,
        description: 'update deployment II',
        retainDeployments: true
        
    })
    const newDeployment = new apigateway.Deployment(this, 'apiDeployment 5', {
        api: restApi,
        description: 'update deployment V',
        retainDeployments: true
        
    })
    const uniqueDeployment = new apigateway.Deployment(this, 'apiDeployment 10', {
        api: restApi,
        description: 'update deployment 10',
        retainDeployments: true
        
    })

    const stageTest = new apigateway.Stage(this, 'staging object', {
        deployment: newDeployment,
        stageName: 'test'
    })
    const stageDev = new apigateway.Stage(this, 'staging dev object', {
        deployment: latestDevDeployment,
        stageName: 'dev'
    })

    //////////////////////////////////////////////////////////


    // restApiCasted.addModel('TestModel', {
    //   schema: {
    //       schema: JsonSchemaVersion.DRAFT4,
    //       title: 'Test Schema',
    //       type: JsonSchemaType.OBJECT,
    //       properties: {
    //           'message': {
    //               type: JsonSchemaType.STRING
    //             }
    //         }
    //     }  
    // })
    // restApiCasted.addModel('ListModel', {
    //     schema: {
    //         schema: JsonSchemaVersion.DRAFT4,
    //         title: 'Test List Schema',
    //         type: JsonSchemaType.OBJECT,
    //         properties: {
    //             'name': {
    //                 type: JsonSchemaType.OBJECT
    //             },
    //             'forms': {
    //                 type:JsonSchemaType.ARRAY,
    //                 items: {
    //                     type: JsonSchemaType.OBJECT,
    //                     properties: {
    //                         'item': {
    //                             type: JsonSchemaType.STRING
    //                         },
    //                         'id': {
    //                             type: JsonSchemaType.INTEGER
    //                         }
    //                     }
    //                 }
    //             }
    //         }

    //     }
    // })


  }
}

import * as core from "@aws-cdk/core";
import * as apigateway from "@aws-cdk/aws-apigateway";
import * as lambda from "@aws-cdk/aws-lambda";
import * as rds from '@aws-cdk/aws-rds';
import * as ec2 from '@aws-cdk/aws-ec2';
import * as s3 from "@aws-cdk/aws-s3";
import * as iam from "@aws-cdk/aws-iam";
import { SecretValue } from "@aws-cdk/aws-rds/node_modules/@aws-cdk/core";


export class TestcdkConstruct extends core.Construct {
  constructor(scope: core.Construct, id: string) {
    super(scope, id);


    const lambdaRuntimes = lambda.Runtime.NODEJS_14_X
    const securityGroupId = 'sg-0fa8cf78'

    const lambdaEnvironmentVariables = {
      CONTROLLED_SUBSTANCE_CODES_DEFAULT: "2,3,4,5,U,NA",
      CONTROLLED_SUBSTANCE_PARAM: "cs-code",
      DEA_CLASS_CODE_DEFAULT: "2,3,4,5,NA",
      DEA_CLASS_PARAM: "dc-code",
      ITEM_STATUS_FLAGS_DEFAULT: "A,O",
      ITEM_STATUS_PARAM: "item-status",
      RDS_DATABASE: "medispan_drug",
      RDS_HOSTNAME: "birdirx-nonprod-db.cykj5d7jrf7z.us-east-2.rds.amazonaws.com",
      RDS_PASSWORD: "	M35fjh$HF8Rs",
      RDS_PORT: "3306",
      RDS_USERNAME: "birdirxnonprod",
      RTX_INDICATOR_CODE_DEFAULT: "O,P",
      TIMEOUT_TOLERANCE_MILLIS: "10"
    }

    const securityGroup = ec2.SecurityGroup.fromSecurityGroupId(this, 'DefaultSecurityGroup', securityGroupId, {
      mutable: false
    })


    const vpc = ec2.Vpc.fromLookup(this, "VPC",
      {
        isDefault: true,
        vpcId: "vpc-ef4fdf84" 
      }
    )

    const vpcSubnets = {
      subnetType: ec2.SubnetType.PUBLIC
    }

    const secret = SecretValue.plainText('admin1234')
    const rdsDatabase = new rds.DatabaseInstance(this, "DrugDatabase", {
        instanceIdentifier: "cdkTestDatabaseInstance",
        databaseName: "cdkTestDatabase",
        credentials: rds.Credentials.fromPassword('admin', secret), // Use password from SSM
        // credentials: rds.Credentials({
        //     username: "admin",
        //     password: "admin1234"
        // }),
        engine: rds.DatabaseInstanceEngine.mysql({
            version: rds.MysqlEngineVersion.VER_8_0_23,
        }),
        instanceType: ec2.InstanceType.of(ec2.InstanceClass.BURSTABLE2, ec2.InstanceSize.MICRO),
        vpc: vpc,
        vpcSubnets: vpcSubnets,
        publiclyAccessible: true,
        securityGroups: [
          securityGroup
        ],
        deleteAutomatedBackups: true        
    })

    const lambdaExecutionRole = new iam.Role(this, 'LambdaRole', 
    {
      // assumedBy: new AccountPrincipal(stack.account)
      assumedBy: new iam.ServicePrincipal('lambda.amazonaws.com'),
      managedPolicies: [
        iam.ManagedPolicy.fromAwsManagedPolicyName('service-role/AWSLambdaVPCAccessExecutionRole'),
      ],
    })

    const mysqlLayer = new lambda.LayerVersion(this, "DrugLayer", {
      code: lambda.Code.fromAsset("resources/lambdas/layers/nodejs"),
      compatibleRuntimes: [lambdaRuntimes]
    })
    const lambdaSharedConfiguration = {
      runtime: lambdaRuntimes,
      layers: [
        mysqlLayer
      ],
      role: lambdaExecutionRole,
      vpc: vpc,
      allowPublicSubnet: true,
      
      securityGroups: [securityGroup],
    }

    const apiLambdaIntegrationSharedConfiguration = {
      allowTestInvoke: true,
    }


    const drugLookupFunction = new lambda.Function(this, "DrugLookupFunction", {
      code: lambda.Code.fromAsset("resources/lambdas/getDrugLookup"),
      handler: "getDrugLookup.getDrugLookup",
      environment: {
        ...lambdaEnvironmentVariables,
        SEARCH_MIN_LENGTH: "3",
        SEARCH_PARAM: "search-name",
      },
      ...lambdaSharedConfiguration
      
    })

    const drugInfoFunction = new lambda.Function(this, "DrugInfoFunction", {
      code: lambda.Code.fromAsset("resources/lambdas/getDrugInfo"),
      handler: "getDrugInfo.getDrugInfo",
      environment: {
        ...lambdaEnvironmentVariables,
        NAME_PARAM: "name",
      },
      ...lambdaSharedConfiguration
    })

    const api = new apigateway.RestApi(this, "TestApi", {
      restApiName: "Test Api CDK",
      description: "This service is the api."
    });
    const drugLookupIntegration = new apigateway.LambdaIntegration(drugLookupFunction, {
      // requestParameters: {
      //   "search-name": ""
      // },
      requestTemplates: { "application/json": '{ "statusCode": "200" }' },

    });
    const drugInfoIntegration = new apigateway.LambdaIntegration(drugInfoFunction, {
      // requestParameters: {
      //   "name": ""
      // },
      
      requestTemplates: { "application/json": '{ "statusCode": "200" }' }
    });

    const drugLookupResource= api.root.addResource('druglookup')
    drugLookupResource.addMethod('GET', drugLookupIntegration, {
      authorizationType: apigateway.AuthorizationType.NONE,
      apiKeyRequired: false
    })
    
    const drugInfoResource= api.root.addResource('drug')
    drugInfoResource.addMethod('GET', drugInfoIntegration, {
      authorizationType: apigateway.AuthorizationType.NONE,
      apiKeyRequired: false
    })

  }
}
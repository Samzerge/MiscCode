import * as cdk from '@aws-cdk/core';
import * as apigateway from "@aws-cdk/aws-apigateway";
import * as cognito from "@aws-cdk/aws-cognito";
import * as iam from "@aws-cdk/aws-iam";
import * as lambda from "@aws-cdk/aws-lambda";
import { Duration } from '@aws-cdk/core';


export class ApiSecurityConstruct extends cdk.Construct {
  public value = 5
  constructor(scope: cdk.Construct, id: string, props?: any) {
    super(scope, id);

    const lambdaRuntime = lambda.Runtime.NODEJS_14_X

    const implementationFunction = new lambda.Function(this, "TestFunction", {
        runtime: lambdaRuntime,
        code: lambda.Code.fromAsset("resources/lambdas/implFunction"),
        handler: "implFunction.implFunction",

    })

    const restApi = new apigateway.RestApi(this, "TestApi", {
      description: "This service is the api.",
    });


    // const apiCognitoAuthorizer = new apigateway.CognitoUserPoolsAuthorizer(this, 'TestCognitoAuthorizer', {
    //   authorizerName: 'TestCognitoAuthorizer',
    //   cognitoUserPools: [
    //     props.userPool
    //   ]
    // });


    const lambdaIntegration = new apigateway.LambdaIntegration(implementationFunction, {
      requestTemplates: { "application/json": '{ "statusCode": "200" }' },

    });


    const testResourceNone = restApi.root.addResource('test')
    const testMethod = testResourceNone.addMethod('GET', lambdaIntegration, {
      apiKeyRequired: false
    })
    const testResource1= restApi.root.addResource('test-identity-auth')
    const testMethod1 = testResource1.addMethod('GET', lambdaIntegration, {
      // authorizer: apiTokenAuthorizer,
      // authorizationType: apigateway.AuthorizationType.CUSTOM,
      apiKeyRequired: false
    })

    // const cognitoTestResource = restApi.root.addResource('test-cognito-auth')
    // const cognitoTestMethod = cognitoTestResource.addMethod('GET', lambdaIntegration, {
    //   authorizer: apiCognitoAuthorizer,
    //   authorizationType: apigateway.AuthorizationType.COGNITO
    // })



    // const apiKey = new apigateway.ApiKey(this, "TestApiKey", {
    //   resources: [restApi],
    //   apiKeyName: 'TestApiKey',

    // })

    // const usagePlan = new apigateway.UsagePlan(this, "TestUsagePlan", {
    //   name: "TestUsagePlan",
    //   quota: {
    //     limit: 10000,
    //     offset: 20,
    //     period: apigateway.Period.MONTH
    //   },
    //   apiKey: apiKey,
    // })


    // usagePlan.addApiStage({
    //   stage: restApi.deploymentStage,
    // })


    // const deployment = new apigateway.Deployment(this, 'Deployment', {
    //   api: restApi
    // })

  }
}

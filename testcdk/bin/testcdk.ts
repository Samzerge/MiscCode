import { LambdaLayerVersionTracker } from './../lib/components/lambda-layer-version-tracker'
import { Test2cdkStack } from './../lib/test2cdk-stack';
import { LambdaLayer } from './../lib/components/lambda-layer';
import 'source-map-support/register';
import * as cdk from '@aws-cdk/core';
import { TestcdkStack } from '../lib/testcdk-stack';
import { ApiHttpIntegrationAuthorizerStack } from '../lib/api-http-integration-authorizer-stack';
import { LambdaLayerCaller } from '../lib/components/lambda-layer-caller';
import { SimpleLambdaStack } from '../lib/components/simple-lambda-stack.ts';
import { LambdaWithExtensionStack } from '../lib/lambda-with-extension.stack';

const app = new cdk.App();

const stackProperties = {
  env: {
    account: '070307939360', 
    region: 'us-east-2'
  }
}
// new TestcdkStack(app, 'TestcdkStack', {
//   env: {
//     account: '070307939360', 
//     region: 'us-east-2'
//   }
//   /* If you don't specify 'env', this stack will be environment-agnostic.
//    * Account/Region-dependent features and context lookups will not work,
//    * but a single synthesized template can be deployed anywhere. */

//   /* Uncomment the next line to specialize this stack for the AWS Account
//    * and Region that are implied by the current CLI configuration. */
//   // env: { account: process.env.CDK_DEFAULT_ACCOUNT, region: process.env.CDK_DEFAULT_REGION },

//   /* Uncomment the next line if you know exactly what Account and Region you
//    * want to deploy the stack to. */
//   // env: { account: '123456789012', region: 'us-east-1' },

//   /* For more information, see https://docs.aws.amazon.com/cdk/latest/guide/environments.html */
// });



// new TestcdkStack(app, 'TestCdkStack', {
//   env: {
//     account: '070307939360', 
//     region: 'us-east-2'
//   },
// })

// new ApiHttpIntegrationAuthorizerStack(app, 'TestStack', stackProperties)
// new Test2cdkStack(app, 'TestCdk2Stack', stackProperties)

// new LambdaLayerVersionTracker(app, `LambdaLayerVersionTrackerStack`,1, stackProperties)
// new LambdaLayerCaller(app, `LambdaLayerCallerStack`,1, stackProperties)

// new SimpleLambdaStack(app, 'SimpleLambdaStack', stackProperties)

new LambdaWithExtensionStack(app, 'LambdaWithExtension', stackProperties)

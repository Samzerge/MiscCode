const utils = require('./utils')
const AWS = require('aws-sdk')

exports.ssmParameterAccess = async (event, context, callback) => {

    context.callbackWaitsForEmptyEventLoop = false

    const SSM = new AWS.SSM()
    // console.log('ssmExist', ssmExist)

    const timeoutToleranceMillis = process.env.TIMEOUT_TOLERANCE_MILLIS

    let res = utils.constants.responseTemplate

    const timer = utils.setAuxTimeout(context, callback, res, timeoutToleranceMillis)
    
    const params1 = {
        Name: process.env.SSM_PARAMETER_RETRIEVE_NAME,
        WithDecryption: false
    }

    const model = {
        'access_token': 'sdaffdsa344$%$fd54',
        'id_token': '12311332',
        'grant_id': '343',
        'expires_in': 7200,
        'token_type': 'Bearer',
        'scope': 'openid',
        'changingValue': process.env.SSM_PARAMETER_VALUE
    }

    const putParams = {
        Name: process.env.SSM_PARAMETER_RETRIEVE_NAME, 
        Value: JSON.stringify(model), 
        DataType: 'text',
        Description: 'The token retrieved from the MedImpact API authorization process',
        Overwrite: true,
        Type: 'String',
        Tier: 'Standard'
      }


    const ssmDeleteParams = {
        Name: process.env.SSM_PARAMETER_RETRIEVE_NAME
    }

    try {
        const ssmParameter = await getSSMParameterPromise(SSM, params1)
        console.log('ssmParameter1', ssmParameter)
    } catch (error) {
        console.log('GET ERROR: ', error)
    }

    try {
        const putResult = await putSSMParameterPromise(SSM, putParams)
        console.log('putResult', putResult)
    } catch (error) {
        console.log('PUT ERROR: ', error)
    }


    
    res.statusCode = 200
    res.body = JSON.stringify({
        message: 'finished'
    })
    utils.finishCall(callback,res, timer)
}

const deleteSSMParameterPromise = (SSM, params) => {

    return new Promise((resolve, reject) => {
        SSM.deleteParameter(params, function(err, data) {
            console.log('DELETE_SSM: data', data)
            console.log('DELETE_SSM: err', err)

            if (err != null) {
                console.log('reject delete')
                reject(err)
            }
    
            console.log('resolving delete')
            resolve(data)
        })
    })
}

const putSSMParameterPromise = (SSM, params) => {
    console.log ('put call')
      
    return new Promise((resolve, reject) => {
        SSM.putParameter(params, function(err, data) {
            console.log('PUTSSM: data', data)
            console.log('PUTSSM: err', err)
    
            if (err != null) {
                console.log('reject put')
                reject(err)
            }
    
            console.log('resolving put')
            resolve(data)
        })
    })

}

const getSSMParameterPromise = (SSM, params) => {


    return new Promise((resolve, reject) => {
        SSM.getParameter(params, function(err, data) {
            console.log('GETSSM: data', data)
            console.log('GETSSM: err', err)

            if (err != null) {
                console.log('rejecting get')
                reject(err)
            }
            
            console.log('resolving get')
            resolve(data)
        })
    })
}
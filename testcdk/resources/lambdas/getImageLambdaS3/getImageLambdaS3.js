const utils = require('./utils')
const AWS = require('aws-sdk')

exports.getImageLambdaS3 = async (event, context, callback) => {
  console.log('getImageLambdaS3')
  
  context.callbackWaitsForEmptyEventLoop = false;

  const bucketPaths = {
    Bucket: process.env.PRIVATE_BUCKET,
    Key: process.env.PRIVATE_KEY
  }
  const timeoutToleranceMillis = process.env.TIMEOUT_TOLERANCE_MILLIS

  let res = utils.constants.responseTemplate

  const timer = utils.setAuxTimeout(context, callback, res, timeoutToleranceMillis)

  try {
    

    // const awsS3Props = {
    //   accessKeyId: retrievedCredentials.accessKeyId,
    //   secretAccessKey: retrievedCredentials.secretAccessKey,
    //   sessionToken: retrievedCredentials.sessionToken,
    //   signatureVersion: 'v4'
    // }
    // console.log('awsS3Props', awsS3Props)
    console.log('bucketPaths', bucketPaths)
    console.log('s3 object with empty constrcutor')
    // const s3Object = new AWS.S3(awsS3Props)
    const s3Object = new AWS.S3()
    
    // const s3Data = await getS3ObjectPromise(s3Object, bucketPaths)
    // console.log('s3Data', s3Data)

    const presignedUrl = await s3Object.getSignedUrlPromise('getObject', bucketPaths)
    console.log('presignedUrl', presignedUrl)
  
    res.statusCode = 200
    res.body = JSON.stringify({

      presignedUrl
    })
    
  } catch(err) {
    console.error('err', err)
    res.statusCode = 500
    res.body = JSON.stringify({
      message: err
    })
  } finally {
  
    utils.finishCall(callback, res, timer)
  }
  
  
}


const getS3ObjectPromise = (s3Object, bucketParams) => {
  return s3Object.getObject(bucketParams, (err, data) => {
    if (err) {
      return err
    }

     return data
  }).promise()
}

const retrieveS3Object = async (callback, res, timer, s3Object, bucketParams) => {
  console.log('retrieveS3Object bucketParams:', bucketParams)



  // const signedUrl = await s3.getSignedUrlPromise('getObject', objectParams)
  // console.log('signedUrl', signedUrl)

  const object =  await s3Object.getObject(bucketParams, (err, data) => {
    console.log('data', data)
    console.error('err', err)
    if (err) {
      res.statusCode = 500
      res.body = err
      return err
      // utils.finishCall(callback, res, timer)
    }

     return data
  }).promise()

  return object
  
}

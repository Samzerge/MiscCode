const utils = require('./utils')
const AWS = require('aws-sdk')


exports.ssmParameterAccess = async (event, context, callback) => {

    context.callbackWaitsForEmptyEventLoop = false
    const timeoutToleranceMillis = process.env.TIMEOUT_TOLERANCE_MILLIS
    const stage = process.env.DEPLOYED_STAGE

    let res = utils.constants.responseTemplate

    const timer = utils.setAuxTimeout(context, callback, res, timeoutToleranceMillis)

    res.statusCode = 500
    res.body = JSON.stringify({
        message: `Hello from an scheduled Lambda in stage" ${stage}`
    })
    utils.finishCall(callback,res, timer)
}
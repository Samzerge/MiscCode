const utils = require('./utils')
const mysql = require('mysql')
const pool = mysql.createPool({
  host     : process.env.RDS_HOSTNAME,
  user     : process.env.RDS_USERNAME,
  password : process.env.RDS_PASSWORD,
  port     : process.env.RDS_PORT,
  database : process.env.RDS_DATABASE
})

exports.getDrugLookup = (event, context, callback) => {
  
  context.callbackWaitsForEmptyEventLoop = false;
  
  const timeoutToleranceMillis = process.env.TIMEOUT_TOLERANCE_MILLIS
  
  const requestParams = {
    searchNameParam: {
      name: process.env.SEARCH_PARAM,
      required: true,
      multipleValues: false,
      jsonFilter: false,
      validations: {
        'minLength': process.env.SEARCH_MIN_LENGTH
      }
    },
    controlledSubstancesParam: {
      name: process.env.CONTROLLED_SUBSTANCE_PARAM,
      required: false,
      multipleValues: true,
      validations: {
        'acceptedValues': ['1', '2', '3', '4', '5', 'U', 'NA']
      }
    },
    itemStatusParam: {
      name: process.env.ITEM_STATUS_PARAM,
      required: false,
      multipleValues: true,
      validations: {
        'acceptedValues': ['A', 'O', 'I', 'Z']
      }
    },
    deaClassParam: {
      name: process.env.DEA_CLASS_PARAM,
      required: false,
      multipleValues: true,
      validations: {
        'acceptedValues': ['1', '2', '3', '4', '5', 'NA']
      }
    },
    rtxParam: {
      name: process.env.RTX_INDICATOR_PARAM,
      required: false,
      multipleValues: true,
      validations: {
        'acceptedValues': ['R', 'O', 'P', 'S']
      }
    }
  }

  let res = utils.constants.responseTemplate
  
  const timer = utils.setAuxTimeout(context, callback, res, timeoutToleranceMillis)
  
  const validationResult = utils.validateRequest(event, res, requestParams)
  if (!validationResult.isValid) {
    clearTimeout(timer)
    callback(null,validationResult.res)
  }  

  const filterJson = utils.buildFilterJsonFromParams(event.multiValueQueryStringParameters, process.env)
  
  pool.query("CALL drug_list_get(?,?)", [event.queryStringParameters[requestParams.searchNameParam.name], JSON.stringify(filterJson)], (err, result, fields) => {
    
    if (err) {
      res.statusCode = 500
      res.body = JSON.stringify(err)
      utils.finishCall(callback,res, timer)

    } else {

      const body = mapResultToModel(result[0])
      res.statusCode = 200
      res.body = JSON.stringify(body)

      utils.finishCall(callback,res, timer)
    }
  })
}



function mapResultToModel(responseObject) {
  return responseObject.map(item => {
    return {
      drugName: item.drug_name
    }
  })
}

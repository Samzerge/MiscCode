const versionedLayerCode = require('/opt/nodejs/versionedLayerCode')

exports.versionedLayersCaller = async (event, context, callback) => {
  
   
    return {
        statusCode: 200,
        body: JSON.stringify(versionedLayerCode.getLayerVersion()),
        headers: {
          "Content-Type": "*/*",
          "Access-Control-Allow-Origin": "*"
        },
        isBase64Encoded: false
    }
  
}
const utils = require('utils')
const axios = require('axios')

exports.getThirdPartyAPI = async (event, context, callback) => {
  
  context.callbackWaitsForEmptyEventLoop = false
  
  const timeoutToleranceMillis = process.env.TIMEOUT_TOLERANCE_MILLIS
  
  const requestParams = { }

  let res = utils.constants.responseTemplate
  
  const timer = utils.setAuxTimeout(context, callback, res, timeoutToleranceMillis)
  
  const validationResult = utils.validateRequest(event, res, requestParams)
  if (!validationResult.isValid) {
    utils.finishCall(callback, validationResult.res, timer)
  }

    try {

        const empty = false
        const url = ''
        const path = ''

        const options = {
            url: url,
            // port: 80,
            headers: {

            },
            method: 'GET'
        }

        const result = await axios.get(...options)

        console.error('result', result)

        if (empty) {
            console.error('err in dbResult', dbResult) 
            res = utils.createErrorObject(res, utils.constants.httpCodes.NOT_FOUND, `Result not found: '${requestParams.drugNameParam.name}: ${event.queryStringParameters[requestParams.ndcParam.name]}'`)
            
        } else {

        // TODO: 
        
        }
    } catch(err) {
        console.error('err', err)
        res.statusCode = 500
        res.body = JSON.stringify({
            message: err
        })
        
    } finally {
        res.statusCode = 200
        res.body = JSON.stringify('Hola mundo')
        utils.finishCall(callback, res, timer)
    }
}
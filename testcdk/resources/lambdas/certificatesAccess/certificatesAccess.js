const utils = require('./utils')
const AWS = require('aws-sdk')

exports.certificatesAccess = async (event, context, callback) => {

    context.callbackWaitsForEmptyEventLoop = false

    const timeoutToleranceMillis = process.env.TIMEOUT_TOLERANCE_MILLIS

    let res = utils.constants.responseTemplate

    const timer = utils.setAuxTimeout(context, callback, res, timeoutToleranceMillis)


    const acm = new AWS.ACM()

    const certificateParams = {
        CertificateArn: 'arn:aws:acm:us-east-2:599822976138:certificate/1a1a7ac4-d810-4ab3-a90e-a1a6c90d06e7'
    }

    acm.getCertificate(certificateParams, function(err, data) {
        if (err) console.log(err, err.stack); // an error occurred
        else     console.log(data);           // successful response
    })



    res.statusCode = 200
    res.body = JSON.stringify({
        message: 'finished'
    })
    utils.finishCall(callback,res, timer)
}

const getACMCertificatePromise = (ACM, params) => {

    return new Promise((resolve, reject) => {
        ACM.getCertificate(params, function(err, data) {
            if (err != null) {
                reject(err)
            }
    
            resolve(data)
        })
    })
}
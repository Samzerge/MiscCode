const https = require('http')
const axios = require('axios')

exports.lambdaWithExtension = async (event, context, callback) => {
  const myUrl = 'http://127.0.0.1:8080/my-cache-value'
  
  try {
    const result = await axios.get(myUrl, {
      params: {
        secretId: 'harcoded1'
      }
    })
    console.log('result', result.data)
    return result.data

  } catch(error) {
    console.log('error', error)
      return error
  }

}

// exports.lambdaWithExtension = function (event, context, callback) {
//     const secretRequestOptions = {
//         url: 'localhost/my-cache-value:8080',
//         method: 'GET',
//     }
//     const myUrl = 'http://127.0.0.1/my-cache-value:8080'

//     axios.get('http://127.0.0.1:8080').then(result => {
//       console.log('result', result)
//       return result
//     }).catch(error => {
//       console.log('error', error)
//       return error
//     }) 
//     // try {
//     //   const result = await axios({...secretRequestOptions})

//     // } catch(error) {
//     //     return error
//     // }

// }

exports.lambdaWithExtension = function (event, context, callback) {

    context.callbackWaitsForEmptyEventLoop = false;
    const options = {
        hostname: 'localhost',
        port: 8080,
        path: '/cache/secret_now',
        method: 'GET'
    }
    const options2 = {
        hostname: 'localhost',
        port: 8080,
        path: '/my-cache-value',
        method: 'GET'
    }


    const req = https.request(options2, res => {
        res.on('data', d => {
            console.log("Response from cache: "+d);
            return d;
        })
    })
    

    req.on('error', error => {
        console.error(error)
    })

    req.end()
};

const request = async (options) => {
    //   const url = url_obj.parse(url_string);
    //   const lib = url.protocol=="https:" ? https : http;
    console.log('options with ', options)
    
      return new Promise((resolve, reject) => {
        const req = https.request((options, res) => {
          if (res.statusCode < 200 || res.statusCode >= 300) {
            return reject(new Error(`Status Code: ${res.statusCode}`));
          }
          const data = [];
          res.on("data", chunk => {
            resolve(chunk)
          });
        });
        req.on("error", error => {
            reject(error)
        });
        req.end();
      });
    }
    
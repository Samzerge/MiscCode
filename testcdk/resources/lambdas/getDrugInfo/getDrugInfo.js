const utils = require('./utils')
const mysql = require('mysql')
const pool = mysql.createPool({
  host     : process.env.RDS_HOSTNAME,
  user     : process.env.RDS_USERNAME,
  password : process.env.RDS_PASSWORD,
  port     : process.env.RDS_PORT,
  database : process.env.RDS_DATABASE
})
exports.getDrugInfo = (event, context, callback) => {
  
  context.callbackWaitsForEmptyEventLoop = false;
  
  const timeoutToleranceMillis = process.env.TIMEOUT_TOLERANCE_MILLIS
  const requestParams = {
    drugNameParam: {
      name: process.env.NAME_PARAM,
      required: true,
      multipleValues: false,
      validations: null
    },
    controlledSubstancesParam: {
      name: process.env.CONTROLLED_SUBSTANCE_PARAM,
      required: false,
      multipleValues: true,
      validations: {
        'acceptedValues': ['1', '2', '3', '4', '5', 'U', 'NA']
      }
    },
    itemStatusParam: {
      name: process.env.ITEM_STATUS_PARAM,
      required: false,
      multipleValues: true,
      validations: {
        'acceptedValues': ['A', 'O', 'I', 'Z']
      }
    },
    deaClassParam: {
      name: process.env.DEA_CLASS_PARAM,
      required: false,
      multipleValues: true,
      validations: {
        'acceptedValues': ['1', '2', '3', '4', '5', 'NA']
      }
    },
    rtxParam: {
      name: process.env.RTX_INDICATOR_PARAM,
      required: false,
      multipleValues: true,
      validations: {
        'acceptedValues': ['R', 'O', 'P', 'S']
      }
    }
  } 

  let res = utils.constants.responseTemplate
  
  const timer = utils.setAuxTimeout(context, callback, res, timeoutToleranceMillis)
  
  const validationResult = utils.validateRequest(event, res, requestParams)
  if (!validationResult.isValid) {
    clearTimeout(timer)
    callback(null,validationResult.res)
  }  

  const filterJson = utils.buildFilterJsonFromParams(event.multiValueQueryStringParameters, process.env)

  pool.query("CALL drug_dosage_form_list_get(?,?)", [event.queryStringParameters[requestParams.drugNameParam.name], JSON.stringify(filterJson)] , (err, result, fields) => {
    
    if (err) {
      res.statusCode = 500
      res.body = JSON.stringify(err)
      utils.finishCall(callback,res, timer)

    } else {
      if (result[0].length <= 0) {
        res.statusCode = 404
        res.body = JSON.stringify({
          code: 404,
          status: "Not Found",
          message: `Result not found: '${event.queryStringParameters[requestParams.drugNameParam.name]}'`
        })
        utils.finishCall(callback,res, timer)
      }
      
      const body = mapResultToModel(event.queryStringParameters[requestParams.drugNameParam.name], result[0])

      res.statusCode = 200
      res.body = JSON.stringify(body)

      utils.finishCall(callback,res, timer)
    }
  })

}

function mapResultToModel(drugName, responseObject) {


  const resultObj = {
    drugName: drugName,
    dosageForms: []
  }

  responseObject.forEach(item => {
    const dosageIndex = resultObj.dosageForms.findIndex(dosage => dosage.dosageForm === item.dosage_form)

    if(dosageIndex > -1) {
      resultObj.dosageForms[dosageIndex].strengths.push({
        strength: item.strength,
        strengthUnit: item.strength_unit_of_measure,
        fullStrength: item.full_strength,
        ddid: item.ddi
      })
    } else {
      resultObj.dosageForms.push({
        dosageForm: item.dosage_form,
        strengths: [{
          strength: item.strength,
          strengthUnit: item.strength_unit_of_measure,
          fullStrength: item.full_strength,
          ddid: item.ddi
        }]
      })
    }
  })

  return resultObj;


}
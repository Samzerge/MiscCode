const constants = {

    responseTemplate: {
        "headers": {
              "Content-Type": "*/*"
        },
        "isBase64Encoded": false
    },
    httpCodes: {
        BAD_REQUEST: 400,
        NOT_IMPLEMENTED: 500,
    }

}

/**
 * Creates an auxiliary function that helps to send an error message when Lambda timeouts
 * @param {*} context 
 * @param {*} callback 
 * @param {*} res 
 * @param {*} timeoutToleranceMillis 
 * @returns 
 */
function setAuxTimeout(context, callback, res, timeoutToleranceMillis){

    const timer = setTimeout(() => {
        res.body = JSON.stringify({
            code: 408,
            status: "Lambda function timeout",
            message: `Lambda function timeout`
        })
        finishCall(callback,res, timer)
    
    }, context.getRemainingTimeInMillis() - timeoutToleranceMillis)
    
    return timer
}

/**
 * Clears timeout and informs lambda that execution has finished
 * @param {*} callback 
 * @param {*} responseObject 
 * @param {*} timer 
 */
function finishCall(callback, responseObject, timer){
    clearTimeout(timer)
    callback(null, responseObject)
}

/**
 * Validates request parameters and configuration parameters
 * @param {*} event 
 * @param {*} res 
 * @param {*} requestParams 
 * @returns 
 */
function validateRequestAndConfig(event, res, requestParams, configParams, configParamsValidations){
    let validationResult = validateRequest(event, res, requestParams)
  
    if (validationResult.isValid) {
      validationResult = validateConfigParams(res, configParams, configParamsValidations)
    }

    return validationResult;
    
}
  
  
/**
 * Validates every parameter accepted in the API
 * @param {*} event 
 * @param {*} paramsArray 
 */
function validateRequest(event, res, requestParams){

    for(const paramKey in requestParams) {
        const param = requestParams[paramKey]
        
        let requestValue = null

        if (event.queryStringParameters != null) {
            requestValue = param.multipleValues ? event.multiValueQueryStringParameters[param.name] : event.queryStringParameters[param.name]
        }

        if(param.required && requestValue == null) {
            return createValidityErrorObject(res, constants.httpCodes.BAD_REQUEST, `Missing required request parameters: '${param.name}'.`)
        }
        
        if (requestValue != null && param.validations != null) {
            for (let validationKey in param.validations) {
                const referenceValue = param.validations[validationKey]

                switch(validationKey) {
                    case 'acceptedValues':

                        if (param.multipleValues) {
                        
                            for(const value of requestValue) {                  
                                if(!referenceValue.includes(value)) {
                                    return createValidityErrorObject(res, constants.httpCodes.BAD_REQUEST, `Parameter '${param.name}' only accepts the following values: [${referenceValue}].`)
                                }
                            }
                        
                        
                        } else {
                            if(event.multiValueQueryStringParameters[param.name].length > 1 || !referenceValue.includes(requestValue)) {
                                return createValidityErrorObject(res, constants.httpCodes.BAD_REQUEST, `Parameter '${param.name}' only accepts 1 of the following values: ${referenceValue}.`)
                            }
                        }
                    break;
                    case 'minLength':
                        if (requestValue.length < referenceValue) {
                            return createValidityErrorObject(res, constants.httpCodes.BAD_REQUEST, `Parameter '${paramKey}' must have at least ${referenceValue} characters.`)
                        }
                    break;
                }
            }
        }
    }
    return {
        isValid: true,
        res: res
    }
}

/**
 * Validates a set of parameters configured on the Lambda function
 * @param {*} res 
 * @param {*} configParamsValues 
 * @param {*} configParamsValidations 
 * @returns 
 */
function validateConfigParams(res, configParamsValues, configParamsValidations){
    for(const paramKey in configParamsValues) {
        const paramValue = configParamsValues[paramKey]

        if (paramValue != null && configParamsValidations != null) {
            if (configParamsValidations[paramKey] != null && configParamsValidations[paramKey]['acceptedValues'] != null) {
                const refValue = configParamsValidations[paramKey]['acceptedValues']
                const valueToArray = paramValue.split(',')
                for(const value of valueToArray) {                  
                    if(!refValue.includes(value)) {
                        return createValidityErrorObject(res, constants.httpCodes.NOT_IMPLEMENTED,  `Server configuration parameter '${paramKey}' only accepts the following values: [${refValue}].`)
                    }
                }
            }
        }
    }

    return {
        isValid: true,
        res: res
    }
}

/**
 * Create json filter for storec procedures based on request param values or environment variables
 * @param {*} multiValueQueryStringParameters  the container for the multiValue Parameters
 * @param {*} env  the container for the enviornment variables
 * @returns 
 */
function buildFilterJsonFromParams(multiValueQueryStringParameters, env) {
    const controlledSubtance = multiValueQueryStringParameters[process.env.CONTROLLED_SUBSTANCE_PARAM]
    const itemStatus = multiValueQueryStringParameters[process.env.ITEM_STATUS_PARAM]
    const deaClass = multiValueQueryStringParameters[process.env.DEA_CLASS_PARAM]
    const rxOtc = multiValueQueryStringParameters[process.env.RTX_INDICATOR_PARAM]
  
    return {
      controlled_substance_code: controlledSubtance == null ? env.CONTROLLED_SUBSTANCE_CODES_DEFAULT : controlledSubtance.join(','),
      item_status_flag: itemStatus == null ? env.ITEM_STATUS_FLAGS_DEFAULT : itemStatus.join(','),
      dea_class_code: deaClass == null ? env.DEA_CLASS_CODE_DEFAULT : deaClass.join(','),
      rx_otc_indicator_code: rxOtc == null ? env.RTX_INDICATOR_CODE_DEFAULT : rxOtc.join(',')
    }
}

function createValidityErrorObject(res, httpCode, message) {
    let status = ''
    switch(httpCode) {
        case constants.httpCodes.BAD_REQUEST:
            status = 'Bad Request'
        break
        case constants.httpCodes.NOT_IMPLEMENTED:
            status = 'Not Implemented'
        break
    }

    res.statusCode = httpCode
    res.body = JSON.stringify({
        code: httpCode,
        status: status,
        message: message
    })

    return {
        isValid: false,
        res: res
    }
}


module.exports.constants = constants
module.exports.setAuxTimeout = setAuxTimeout
module.exports.finishCall = finishCall
module.exports.validateRequestAndConfig = validateRequestAndConfig
module.exports.validateRequest = validateRequest
module.exports.validateConfigParams = validateConfigParams
module.exports.buildFilterJsonFromParams = buildFilterJsonFromParams
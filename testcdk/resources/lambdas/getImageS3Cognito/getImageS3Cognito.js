const utils = require('./utils')
const AWS = require('aws-sdk')

exports.getImageS3Cognito = async (event, context, callback) => {
  console.log('getImageS3Cognito')
  
  context.callbackWaitsForEmptyEventLoop = false;

  // const accessKeyId = 'AKIARAXVOAQQPXNULVEH'
  // const secretAccessKey = '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'
  
  const timeoutToleranceMillis = process.env.TIMEOUT_TOLERANCE_MILLIS
  const requestParams = {

  }

  let res = utils.constants.responseTemplate
  
  // const timer = utils.setAuxTimeout(context, callback, res, timeoutToleranceMillis)
  
  const validationResult = utils.validateRequest(event, res, requestParams)
  if (!validationResult.isValid) {
    clearTimeout(timer)
    return validationResult.res
  }  

  const bucketPaths = {
    Bucket: process.env.PRIVATE_BUCKET,
    Key: process.env.PRIVATE_KEY
  }


  AWS.config.region = process.env.COGNITO_REGION
  AWS.config.credentials = new AWS.CognitoIdentityCredentials({
      IdentityPoolId: process.env.COGNITO_IDENTITY_POOL_ID,
  })


  try {
    

    const retrievedCredentials = await getCredentialsPromise(AWS.config.credentials)
    const awsS3Props = {
      accessKeyId: retrievedCredentials.accessKeyId,
      secretAccessKey: retrievedCredentials.secretAccessKey,
      sessionToken: retrievedCredentials.sessionToken,
      signatureVersion: 'v4'
    }
    console.log('awsS3Props', awsS3Props)
    console.log('bucketPaths', bucketPaths)
    const s3Object = new AWS.S3(awsS3Props)
    // const s3Object = new AWS.S3()
    
    // const s3Data =  await s3Object.getObject(bucketPaths, handleS3ObjectRetrieval)
    const s3Data = await getS3ObjectPromise(s3Object, bucketPaths)
    console.log('s3Data', s3Data)
    const presignedUrl = await s3Object.getSignedUrlPromise('getObject', bucketPaths)
    console.log('presignedUrl', presignedUrl)
    // privateS3Object.getSignedUrlPromise()
  
    res.statusCode = 200
    res.body = JSON.stringify({
      // url: s3Data.presignedUrl,
      presignedUrl
    })
    
  } catch(err) {
    console.error('err', err)
    res.statusCode = 500
    res.body = JSON.stringify({
      message: err
    })
  } finally {
  
    utils.finishCall(callback, res, timer)
  }
  
  
}

const getCredentialsPromise = (AWSCredentials) => {
  console.log('getCredentialsPromise')
  return new Promise((resolve, reject) => {
    AWSCredentials.get(err => {
      if (err){
        reject(err)
      } 

      resolve({
        accessKeyId: AWSCredentials.accessKeyId,
        secretAccessKey: AWSCredentials.secretAccessKey,
        sessionToken: AWSCredentials.sessionToken
      })
    })
  })
}

const getS3ObjectPromise = (s3Object, bucketParams) => {
  console.log('getS3ObjectPromise')
  return s3Object.getObject(bucketParams, (err, data) => {
    if (err) {
      return err
    }

     return data
  }).promise()
}

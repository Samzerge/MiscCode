const utils = require('./utils')
const aws = require('aws-sdk')

exports.getImageAndInfo = async (event, context, callback) => {
  
  context.callbackWaitsForEmptyEventLoop = false;

  const accessKeyId = 'AKIARAXVOAQQPXNULVEH'
  const secretAccessKey = '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'
  // const accessKeyId = 'AKIARAXVOAQQPXNULVEH'
  // const secretAccessKey = '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'
  
  const timeoutToleranceMillis = process.env.TIMEOUT_TOLERANCE_MILLIS
  const requestParams = {
  } 

  let res = utils.constants.responseTemplate
  
  const timer = utils.setAuxTimeout(context, callback, res, timeoutToleranceMillis)
  
  const validationResult = utils.validateRequest(event, res, requestParams)
  if (!validationResult.isValid) {
    clearTimeout(timer)
    callback(null,validationResult.res)
  }  

  const privateBucket = {
    Bucket: process.env.PRIVATE_BUCKET,
    Key: process.env.PRIVATE_KEY
  }

  const publicBucket = {
    Bucket: process.env.PUBLIC_BUCKET,
    Key: process.env.PUBLIC_KEY
  }

  const publicS3Object = new aws.S3()
  const privateS3Object = new aws.S3({
    accessKeyId: accessKeyId,
    secretAccessKey: secretAccessKey
  })

  try {

      /**
   * This action is useful to determine if a bucket exists and you have permission to access it. The action returns a 200 OK if the bucket exists and you have permission to access it. If the bucket does not exist or you do not have permission to access it, the HEAD request returns a generic 404 Not Found or 403 Forbidden code. A message body is not included, so you cannot determine the exception beyond these error codes. To use this operation, you must have permissions to perform the s3:ListBucket action. The bucket owner has this permission by default and can grant this permission to others. For more information about permissions, see Permissions Related to Bucket Subresource Operations and Managing Access Permissions to Your Amazon S3 Resources.
   */
  // headBucket(params: S3.Types.HeadBucketRequest, callback?: (err: AWSError, data: {}) => void): Request<{}, AWSError>;

    const headBucket = await headBucketPromise(privateS3Object, {
      Bucket: privateBucket.Bucket
    })
    console.log('headBucket', headBucket)
    const headObject = await headObjectPromise(privateS3Object, {
      Bucket: privateBucket.Bucket+'temp',
      Key: privateBucket.Key
    })
    console.log('headObject', headObject)
    
    const privateSignedUrl = await privateS3Object.getSignedUrlPromise('getObject', privateBucket)
    console.log('privateSignedUrl', privateSignedUrl)
    
    const privateS3Data = await retrieveS3Object(callback, res, timer, privateS3Object, privateBucket)
    // const publicS3Data = await retrieveS3Object(callback, res, timer, publicS3Object, publicBucket)
    console.log('privateS3Data', privateS3Data)
    // console.log('publicS3Data', publicS3Data)
    const privateBase64String= privateS3Data.Body.toString('base64');
    // const publicBase64String= publicS3Data.Body.toString('base64');
    const privateImgSrcString = "data:image/jpeg;base64,"+privateBase64String
    // const publicImgSrcString = "data:image/jpeg;base64,"+publicBase64String

  } catch (error) {
    console.error('ERROR: ', error)
  }
  

  

  
  const sampleResponse = [
    
    {
      dosageForm: 'Tablet',
      strengths: [{
        strength: 20,
        strengthUnit: 'mg',
        // image: privateImgSrcString,
        // signedUrl: privateSignedUrl
      }]  
    },
    // {
    //   dosageForm: 'Syrup',
    //   strengths: [{
    //     strength: 2,
    //     strengthUnit: 'ml',
    //     image: publicImgSrcString,
    //     signedUrl: ''
    //   }]  
    // },
  ]
  
  // const body = mapResultToModel(sampleResponse)
  

  // const bodyData = data.Body.toString('utf-8')




  res.statusCode = 200
  res.body = JSON.stringify(sampleResponse)

  utils.finishCall(callback,res, timer)

}

const headBucketPromise = async (s3Object, params) => {
  return await s3Object.headBucket(params, (err, data) => {
    console.log('headBucketPromise data: ', data)
    console.error('headBucketPromise err: ', err)
    if (err) {
      // res.statusCode = 500
      // res.body = err
      return err
      // utils.finishCall(callback, res, timer)
    }

     return data
  }).promise()
}
const headObjectPromise = async (s3Object, params) => {
  return await s3Object.headObject(params, (err, data) => {
    console.log('headObjectPromise data: ', data)
    console.error('headObjectPromise err: ', err)
    if (err) {
      // res.statusCode = 500
      // res.body = err
      return err
      // utils.finishCall(callback, res, timer)
    }

     return data
  }).promise()
}

const retrieveS3Object = async (callback, res, timer, s3Object, bucketParams) => {
  console.log('retrieveS3Object bucketParams:', bucketParams)



  // const signedUrl = await s3.getSignedUrlPromise('getObject', objectParams)
  // console.log('signedUrl', signedUrl)

  const object =  await s3Object.getObject(bucketParams, (err, data) => {
    // console.log('data', data)
    // console.error('err', err)
    if (err) {
      res.statusCode = 500
      res.body = err
      return err
      // utils.finishCall(callback, res, timer)
    }

     return data
  }).promise()

  return object
  
}

function mapResultToModel(sampleResponse) {

  sampleResponse.forEach(item => {
    const dosageIndex = resultObj.dosageForms.findIndex(dosage => dosage.dosageForm === item.dosage_form)

    if(dosageIndex > -1) {
      resultObj.dosageForms[dosageIndex].strengths.push({
        strength: item.strength,
        strengthUnit: item.strength_unit_of_measure,
        fullStrength: item.full_strength,
        ddid: item.ddi
      })
    } else {
      resultObj.dosageForms.push({
        dosageForm: item.dosage_form,
        strengths: [{
          strength: item.strength,
          strengthUnit: item.strength_unit_of_measure,
          fullStrength: item.full_strength,
          ddid: item.ddi
        }]
      })
    }
  })

  return resultObj;


}
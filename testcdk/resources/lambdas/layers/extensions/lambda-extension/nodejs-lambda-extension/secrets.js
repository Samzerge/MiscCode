#!/usr/bin/env node
const yaml = require('js-yaml');
const fs = require('fs');
const AWS = require('aws-sdk');
const express = require("express");
const http = require('http');
const app = express();
const url = require('url');
const file = "/var/task/config.yaml";
const PORT = 8080;
const ADDRESS = '127.0.0.1'

async function cacheSecrets() {
    console.log('cacheSecrets')
    fs.open(file, 'r', (err, fd) => {
        if (err) {
            if (err.code === 'ENOENT') {
                console.log('Config.yaml doesnt exist so no parsing required');
                return;
            }
        }
        readFile();
    });
}

var secretsCache = {};
var cacheLastUpdated;

async function readFile() {
    console.log('readFile')
    secretsCache['harcoded1'] = 'Bleach and One Piece';
    secretsCache['harcoded2'] = 'Final Fantasy and Kingdom Hearts';
    // Read the file
    try {
        const awsSecretManager = new AWS.SecretsManager();
        // var fileContents = fs.readFileSync(file, 'utf8');
        var fileContents = fs.readFileSync(file, 'utf8');
        var data = yaml.safeLoad(fileContents);
        secretsCache['harcoded'] = 'Bleach and One Piece';
        console.log('data', data)

        if (data != null) {
            var secretManagers = data.SecretManagers;
            console.log('secretManagers', secretManagers)
            if (secretManagers != null) {
                for (var i = 0; i < secretManagers.length; i++) {
                    var secrets = secretManagers[i].secrets;
                    for (var j = 0; j < secrets.length; j++) {
                        var secretName = secrets[j];
                        try {
                            // Read secrets from SecretManager
                            const secretResponse = await awsSecretManager.getSecretValue({SecretId: secretName}).promise();
                            console.log('secretResponse', secretResponse)
                            secretsCache[secretName] = secretResponse.SecretString;
                        } catch (e) {
                            console.log("Error while getting secret name " + secretName);
                        }
                    }
                }
                
                // Read timeout from environment variable and set expiration timestamp
                console.log('process.env.CACHE_TIMEOUT', process.env.CACHE_TIMEOUT)
                var timeOut = parseInt(process.env.CACHE_TIMEOUT || 10);
                var s = new Date();
                s.setMinutes(s.getMinutes() + timeOut);
                cacheLastUpdated = s;

            }
        } else {
            console.log('data was null')
        }
    } catch (e) {
        console.error(e);
    } finally {
        // console.log('secretsCache', secretsCache)

    }
}

async function processPayload(req, res) {
    console.log('processPayload')
    if (new Date() > cacheLastUpdated) {
        await readFile();
        console.log("Cache update is complete")
    }

    var secretName = req.params['name'];
    var secretValue = secretsCache[secretName];
    res.setHeader("Content-Type", "application/json");
    res.status(200);
    res.end(secretValue);
}
async function processMyPayload(req, res) {
    console.log('processMyPayload')

    const secretName = 'harcoded1'
                // const secretName = request.params['harcoded1']
    const secretValue = secretsCache[secretName]
    res.setHeader("Content-Type", "application/json");
    res.status(200);
    res.end(secretValue);
}

async function startHttpServer() {
    console.log('startHttpServer with express')
    app.get("/cache/:name", function (req, res) {
        console.log('received cache/:name request')
        return processPayload(req, res);
    });
    app.get("/my-cache-value", function (req, res) {
        console.log('received my cahche value request')
        return processMyPayload(req, res);
    });
    
    app.listen(PORT, ADDRESS, function (error) {
        if (error) throw error
        console.log("Server created Successfully on PORT", PORT)
    });
    

    console.log(`Listening for requests at http://${ADDRESS}:${PORT}`);
}


function startHttpServer_1() {
    console.log('startHttpServer with http')
    const logsQueue = [];
    // init HTTP server for the Logs API subscription
    const server = http.createServer(function(request, response) {

        // console.log('-----------------------')
        // console.log('request')
        // console.dir(request)
        // console.log('-----------------------')
        console.log('request.url', request.url)
        console.log('request.params', request.params)
        const queryObject = url.parse(request.url,true).query;
        console.log('queryObject', queryObject);
        console.dir(queryObject);
        if (request.method === 'GET') {
            console.log('GET');
            console.log('secretsCache', secretsCache);
            



            if (request.url === '/my-cache-value') {
                console.log('received my cache value request')
                
                // if (request.query.secretId != null) {
                //     console.log('received secret id in params')
                //     console.log('request.query.secretId', request.query.secretId)
                // }

                const secretName = 'harcoded1'
                // const secretName = request.params['harcoded1']
                const secretValue = secretsCache[secretName]
                const responseObject = JSON.stringify({
                    obj: secretValue
                })
                response.writeHead(200, {
                    "Content-Type": "application/json"
                });
                response.end(responseObject)

            } else {
                throw Error('Url not implemented')
            }

        } else {
            throw Error('Method not implemented')
        }
//         if (request.method == 'POST') {
//             var body = '';
//             request.on('data', function(data) {
//                 body += data;
//             });
//             request.on('end', function() {
//                 console.log('Logs listener received: ' + body);
//                 try {
//                     let batch = JSON.parse(body);
//                     if (batch.length > 0) {
//                         logsQueue.push( ...batch );
//                     }
//                 } catch(e) { 
//                     console.log("failed to parse logs"); 
//                 }
//                 response.writeHead(200, {})
//                 response.end("OK")
//             });
//         } else {
// ;
//         }
    });
    
    server.listen(PORT, ADDRESS);
    console.log(`Listening for requests at http://${ADDRESS}:${PORT}`);
    return { logsQueue, server };
}


module.exports = {
    cacheSecrets,
    startHttpServer
};
